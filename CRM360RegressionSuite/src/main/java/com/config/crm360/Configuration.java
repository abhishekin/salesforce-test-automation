package com.config.crm360;

import org.testng.ITestContext;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.common.util.BrowserUtil;
import com.common.util.CleanupUtil;

public class Configuration {

	@BeforeSuite()
	public void setup(ITestContext context) {
		System.out.println("Running BeforeSuite: " + context.getSuite().getName());
		//CleanupUtil.cleanupAppData();
		CleanupUtil.archiveResults();
		CleanupUtil.cleanupFolder("results/");
		CleanupUtil.cleanupFolder("results/screenshots/");
	}

	@AfterSuite()
	public void teardown() {
		BrowserUtil.closeBrowser();
	}
}
