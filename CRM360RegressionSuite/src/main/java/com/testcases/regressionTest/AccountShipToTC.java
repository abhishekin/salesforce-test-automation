package com.testcases.regressionTest;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.common.util.ResultUtil;
import com.common.util.SuiteUtil;
import com.common.util.TestDataUtil;
import com.scripts.crm360.AccountCreateScript;
import com.scripts.crm360.AccountVerifyScript;
import com.scripts.crm360.LogoutScript;
import com.scripts.crm360.UserSelectionScript;

public class AccountShipToTC {
	
String excelSheet = "AccountShipToCreate";


	@BeforeTest
	public void caseToRun() throws IOException {
		String execute_Y_or_NO_flag = TestDataUtil.testData.getCellData("TestCaseConfig", "Execute(Y/N)", 2);
		SuiteUtil.checkTestCaseToRun(execute_Y_or_NO_flag);
		
	}
	
	@Test(priority=1)
	public void createAccountTestCase() throws Exception {
		ResultUtil.test = ResultUtil.reporter.startTest("TC01-CRM360 Regression - Create Ship To Account with PE Sales user");
		System.out.println("TC01-CRM360 Regression - Create Ship To Account with PE Sales user");
			if (TestDataUtil.testData.getCellData(excelSheet, "ExecuteTestData (Y/N)", 2)
					.equalsIgnoreCase("Y")) {
				String profileName = TestDataUtil.testData.getCellData(excelSheet, "ProfileName", 2);
				String userName = TestDataUtil.testData.getCellData(excelSheet, "UserName", 2);
				UserSelectionScript.selectUser(profileName, userName);
				AccountCreateScript.createShipToAccount(excelSheet, 2);
				AccountCreateScript.enterAccountInformation(excelSheet, 2);
				AccountCreateScript.enterDescription(excelSheet, 2);
				AccountCreateScript.enterShippingAddress(excelSheet, 2);
				AccountCreateScript.saveAccount();
				AccountVerifyScript.verifyShipToAccount(excelSheet, 2);
				LogoutScript.performUserLogout();
			}
	
	ResultUtil.reporter.endTest(ResultUtil.test);
		ResultUtil.reporter.flush();
	}
}

