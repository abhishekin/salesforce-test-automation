package com.testcases.regressionTest;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.common.util.ResultUtil;
import com.common.util.SuiteUtil;
import com.common.util.TestDataUtil;
import com.scripts.crm360.LeadCreateScript;
import com.scripts.crm360.LogoutScript;
import com.scripts.crm360.UserSelectionScript;

public class LeadCreateTC {
	
String excelSheet = "LeadCreate";

//Demo commit
	@BeforeTest
	public void caseToRun() throws IOException {
		String execute_Y_or_NO_flag = TestDataUtil.testData.getCellData("TestCaseConfig", "Execute(Y/N)", 4);
		System.out.println(execute_Y_or_NO_flag);
		SuiteUtil.checkTestCaseToRun(execute_Y_or_NO_flag);
		
	}
	
	@Test(priority=1)
	public void createLeadTestCase() throws Exception {
		ResultUtil.test = ResultUtil.reporter.startTest("TC02-CRM360 Regression - Create Lead with PE Sales user");
		System.out.println("TC02-CRM360 Regression - Create Lead with PE Sales user");
			if (TestDataUtil.testData.getCellData(excelSheet, "ExecuteTestData (Y/N)", 2)
					.equalsIgnoreCase("Y")) {
				String profileName = TestDataUtil.testData.getCellData(excelSheet, "ProfileName", 2);
				String userName = TestDataUtil.testData.getCellData(excelSheet, "UserName", 2);
				UserSelectionScript.selectUser(profileName, userName);
				LeadCreateScript.enterLeadInformation(excelSheet, 2);
				LeadCreateScript.enterAddress(excelSheet, 2);
				LeadCreateScript.enterSystemInformation(excelSheet, 2);
				LeadCreateScript.saveNewLead();
				LogoutScript.performUserLogout();
			}
	

	ResultUtil.reporter.endTest(ResultUtil.test);
		ResultUtil.reporter.flush();
	}
}

