package com.pages.crm360;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.common.misc.Sync;
import com.common.toolbox.Button;

public class CommonButtonPage {

	WebDriver driver;

	public CommonButtonPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}

	@FindBy(xpath = "//div[text()='New']")
	WebElement NewButton;

	@FindBy(xpath = ".//button[@title='Save']")
	WebElement SaveButton;

	@FindBy(xpath = "//*/span[text()='Next']")
	WebElement NextButton;

	@FindBy(xpath = "//a[@title='Details']")
	WebElement detailsTab;

	@FindBy(xpath = ".//*[@class='triggerLinkTextAndIconWrapper slds-p-right--x-large']")
	WebElement listView;

	@FindBy(xpath = "//li[@data-aura-class='forceVirtualAutocompleteMenuOption']/a/span[text()='My Non-GSAP Accounts']")
	WebElement myNonGSAPAccounts;

	public boolean clickListView() {
		Sync.waitForObject(driver, "listView", listView);
		return Button.click("List View", listView);
	}

	public boolean clickMyNGSAPAccount() {
		Sync.waitForObject(driver, "myNonGSAPAccounts", myNonGSAPAccounts);
		return Button.click("My NonGSAP Accounts", myNonGSAPAccounts);
	}

	public boolean clickNewAccount() {
		Sync.waitForObject(driver, "New Account", NewButton);
		return Button.click("New Account", NewButton);
	}

	public boolean clickNewLead() {
		Sync.waitForObject(driver, "New Lead", NewButton);
		return Button.click("New Lead", NewButton);
	}

	public boolean clickSaveNewLead() {
		Sync.waitForObject(driver, "Save New Lead", SaveButton);
		return Button.click("Save New Lead", SaveButton);
	}

	public boolean clickSaveNewAccount() {
		Sync.waitForObject(driver, "Save New Account", SaveButton);
		return Button.click("Save New Account", SaveButton);
	}

	public boolean clickNext() {
		Sync.waitForObject(driver, "Next", NextButton);
		return Button.click("Next", NextButton);
	}

	public boolean clickDetailsTab() {
		Sync.waitForObject(driver, "Details Tab", detailsTab);
		return Button.click("Details Tab", detailsTab);
	}

}
