package com.pages.crm360;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.common.toolbox.Button;
import com.common.toolbox.Textbox;

/**
 * Created by Apeksha.Srivastava on 6/27/2016.
 */
public class LoginPage {

	WebDriver driver;

	@FindBy(id = "username")
	WebElement txtUsername;

	@FindBy(id = "password")
	WebElement txtPassword;

	@FindBy(id = "Login")
	WebElement btnLogin;

	public LoginPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}

	public boolean enterUsername(String strUsername) {
		return Textbox.enterValue("Username", txtUsername, strUsername);
	}

	public boolean enterPassword(String strPassword) {
		return Textbox.enterPassword("Password Field", txtPassword, strPassword);
	}

	public boolean clickLoginButton() {
		return Button.click("Login", btnLogin);
	}
}
