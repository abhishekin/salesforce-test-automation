package com.pages.crm360;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.common.misc.Sync;
import com.common.toolbox.Button;
import com.common.toolbox.Frames;
import com.common.toolbox.Link;

public class UserSelectionPage {

	WebDriver driver;

	@FindBy(id = "//div[@class='rolodex']/a")
	WebElement alphabetIndex;
	
	@FindBy(xpath = ".//*[@title='Login']")
	WebElement loginBtn;

	@FindBy(xpath = ".//*[@id='oneHeader']/div[1]/div/span[contains(text(),'Logged in as')]")
	WebElement logedInAs;

	public UserSelectionPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}

	
	public boolean selectAlphabetIndex(String userName) {
		Frames.switchToFrame(driver, "//iframe[contains(@id,'vfFrameId_')]");
		Sync.waitForObject(driver, "Alphabet to Select", By.xpath("//div[@class='rolodex']/a/span"));
		String userNameInCaps = userName.toUpperCase();
		char ch = userNameInCaps.charAt(0);
		WebElement alphabetToSelect = driver
				.findElement(By.xpath("//div[@class='rolodex']/a/span[text()='" + ch + "']"));
		return Link.click("Alphabet To Select", alphabetToSelect);
	}

	public boolean selectUser(String userName) {
		Frames.switchToFrame(driver, "//iframe[@title='CRM PE Sales ~ Salesforce - Unlimited Edition']");
		Sync.waitForObject(driver, "User to Select", By.xpath(".//*[@id='ResetForm']/div[2]/table/tbody/tr[2]/th[1]/a"));
		boolean status = false;
		//List<WebElement> userNames = driver.findElements(By.xpath(".//*[@id='ResetForm']/div/table/tbody/tr[2]/th[1]/a"));
		WebElement userNames = driver.findElement(By.xpath(".//*[@id='ResetForm']/div/table/tbody/tr[2]/th[1]/a"));
		System.out.println(userNames.getText());
		/*Iterator<WebElement> itr = userNames.iterator();
		while (itr.hasNext()) {
			WebElement user = itr.next();
			if (user.getText().equals(userName)) {
				System.out.println("wp"+user.getText());
				System.out.println(userName);*/
				//status = Link.click(userName, user);
		status = Link.click("userName", userNames);
				//break;
			//}
		//}
		return status;
	}
	
	public boolean clickLoginButton() {
		Sync.waitForPageLoad(driver);
		Frames.switchToFrame(driver, "//iframe[contains(@id,'vfFrameId_')]");
		return Button.click("Login", loginBtn);

	}
	
	public boolean waitForUserToGetLoggedin() {
		Sync.waitForPageLoad(driver);
		return Sync.waitForObject(driver, "Wait for Loggedin As", logedInAs);

	}
	
}
