package com.pages.crm360;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.common.misc.Sync;
import com.common.toolbox.Link;
import com.common.toolbox.Menu;
import com.common.toolbox.Textbox;

public class GlobalHeader {

	WebDriver driver;

	@FindBy(id = "221:0;p")
	WebElement searchBox;

	@FindBy(xpath=".//*[@class='icon-settings-component icon-settings-bolt']")
	WebElement setupIcon;
	
	@FindBy(xpath=".//*[@id='all_setup_home']/a/span/span[2]")
	WebElement setupLink;

	@FindBy(xpath = ".//*[@id='oneHeader']/div[1]/div/a")
	WebElement logoutLink;


	public GlobalHeader(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}

	public boolean enterSearchText(String strValue) {
		Sync.waitForPageLoad(driver);
		Sync.waitForObject(driver, "Search", searchBox);
		return Textbox.enterValue("Search", searchBox, strValue);
	}

	public boolean clickSetupIcon() {
		Sync.waitForObject(driver, "Setup Icon", setupIcon);
		return Link.click("Setup Icon", setupIcon);
	}

	public boolean clickSetupLink() {
		Sync.waitForObject(driver, "Setup Link", setupLink);
		return Link.click("Setup Link", setupLink);
	}

	public boolean clickLogoutLink() {
		Sync.waitForObject(driver, "Logout", logoutLink);
		return Link.click("Logout", logoutLink);
	}
	
	//String oldTab = driver.getWindowHandle();
	
	public boolean switchToNewTab() {
		Sync.waitForPageLoad(driver);
		return Menu.switchToNewTab(driver, "New Tab");
	}

}
