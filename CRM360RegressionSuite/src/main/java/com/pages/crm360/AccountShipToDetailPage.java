package com.pages.crm360;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.common.toolbox.Textbox;

public class AccountShipToDetailPage {

	WebDriver driver;

	public AccountShipToDetailPage(WebDriver driver) {
		// super(driver);
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}
	// Ship To

	@FindBy(xpath = "//div/div[1]/div[1]/div/div[2]/span/span[@class='uiOutputText']")
	private WebElement accountName;

	@FindBy(xpath = "//div/div[1]/div[2]/div/div[2]/span/span")
	private WebElement phone;
	
	@FindBy(xpath = "//div[@class='recordTypeName slds-grow']/span")
	private WebElement accountRecordType;

	@FindBy(xpath = "//div/div[2]/div[2]/div/div[2]/span/span")
	private WebElement fax;

	@FindBy(xpath = ".//div/div[3]/div[1]/div/div[2]/span")
	private WebElement roles;

	@FindBy(xpath = "//div/div[3]/div[2]/div/div[2]/span")
	private WebElement website;

	@FindBy(xpath = ".//div/div[4]/div[1]/div/div[2]/span")
	private WebElement parentAccount;

	@FindBy(xpath = "//div/div[4]/div[2]/div/div[2]/span/div/a")
	private WebElement aeResponsible;

	@FindBy(xpath = ".//div/div[5]/div[1]/div/div[2]/span/span")
	private WebElement productLine;

	@FindBy(xpath = ".//label[span[text()='CRC Responsible']]//following-sibling::div//input")
	private WebElement crcResponsible;

	@FindBy(xpath = ".//*[@class='street slds-truncate forceOutputAddressText']")
	private WebElement shippingStreet;

	@FindBy(xpath = ".//*[@class='desktop municipality slds-truncate forceOutputAddressText']/span[1]")
	private WebElement shippingCity;

	@FindBy(xpath = ".//*[@class='desktop municipality slds-truncate forceOutputAddressText']/span[3]")
	private WebElement shippingState;

	@FindBy(xpath = ".//*[@class='desktop municipality slds-truncate forceOutputAddressText']/span[4]")
	private WebElement shippingZip;

	@FindBy(xpath = ".//*[@class='desktop municipality slds-truncate forceOutputAddressText']/span[5]")
	private WebElement shippingCountry;
	
	@FindBy(xpath = "//span[@class='uiOutputTextArea']")
	private WebElement description;

	public boolean verifyAccountName(String AccountName) {
		return Textbox.verifyText("Description/Name", accountName, AccountName);
	}

	public boolean verifyPhone(String Phone) {
		return Textbox.verifyText("Phone", phone, Phone);
	}

	public boolean verifyFax(String Fax) {
		return Textbox.verifyText("Fax", fax, Fax);
	}
	
	public boolean verifyAccountRecordType(String AccountRecordType) {
		return Textbox.verifyText("AccountRecordType", accountRecordType, AccountRecordType);
	}
	
	public boolean verifyRoles(String Roles) {
		return Textbox.verifyText("Roles", roles, Roles);
	}
	
	public boolean verifyWebsite(String Website) {
		return Textbox.verifyText("WebSites", website, Website);
	}

	public boolean verifyParentAccount(String ParentAccount) {
		return Textbox.verifyText("ParentAccount", parentAccount, ParentAccount);
	}
	
	public boolean verifyAEResponsible(String AEResponsible) {
		return Textbox.verifyText("AE Responsible", aeResponsible, AEResponsible);
	}
	
	public boolean verifyProductLine(String ProductLine) {
		return Textbox.verifyText("Product Line", productLine, ProductLine);
	}
	
	public boolean verifyCRCResponsible(String CRCResponsible) {
		return Textbox.verifyText("CRC Responsible", crcResponsible, CRCResponsible);
		
	}
	public boolean verifyShippingStreet(String ShippingStreet) {
		return Textbox.verifyText("ShippingStreet", shippingStreet, ShippingStreet);
	}
	
	public boolean verifyShippingCity(String ShippingCity) {
		return Textbox.verifyText("ShippingCity", shippingCity, ShippingCity);
	}
	
	public boolean verifyShippingState(String ShippingState) {
		return Textbox.verifyText("ShippingState", shippingState, ShippingState);
	}
	
	public boolean verifyShippingZip(String ShippingZip) {
		return Textbox.verifyText("ShippingZip", shippingZip, ShippingZip);
	}
	
	public boolean verifyShippingCountry(String ShippingCountry) {
		return Textbox.verifyText("ShippingCountry", shippingCountry, ShippingCountry);
	}
	
	public boolean verifyDescription(String Description) {
		return Textbox.verifyText("Description", description, Description);
	}
}
