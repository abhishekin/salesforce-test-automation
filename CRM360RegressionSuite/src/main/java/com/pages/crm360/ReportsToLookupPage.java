package com.pages.crm360;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.common.toolbox.Button;
import com.common.toolbox.Textbox;

public class ReportsToLookupPage {

	WebDriver driver;

	public ReportsToLookupPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}

	@FindBy(xpath = "//input[@id='lksrch']")
	WebElement search;

	@FindBy(name = "go")
	WebElement goButton;

	public boolean performSearch(String strValue) {
		System.out.println(driver.getWindowHandle());
		Textbox.enterValue("Search text", search, strValue);
		return Button.click("Go", goButton);
	}

	public void switchToFrame() {
		driver.switchTo().frame("searchFrame");
	}

	//Add code to select the Reports To item
	
	/*
	 * public boolean selectContact(String shellSFUserContactName) {
	 * Sync.waitForPageLoad(driver); driver.switchTo().parentFrame();
	 * driver.switchTo().frame("resultsFrame"); WebElement
	 * shellSFUserContactNameElement = driver
	 * .findElement(By.xpath("//th/a[text()='" + shellSFUserContactName +
	 * "']")); return Link.click("Shell SF User Contact Name",
	 * shellSFUserContactNameElement); }
	 */
	public void switchToParentWindow() {
		//driver.switchTo().window(ContactNewPage.parentWindowId);
	}

}
