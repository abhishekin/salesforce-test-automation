package com.pages.crm360;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.common.misc.Sync;
import com.common.toolbox.Dropdown;
import com.common.toolbox.Link;
import com.common.toolbox.Textbox;

/**
 * Created by AbhishekTiwari on 28/Aug/2017.
 */

public class AccountSoldToCreatePage {

	WebDriver driver;

	public AccountSoldToCreatePage(WebDriver driver) {
		// super(driver);
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}

	@FindBy(xpath = ".//*[@class='input uiInput uiInputText uiInput--default uiInput--input']")
	private WebElement accountName;

	@FindBy(xpath = ".//label[span[text()='Phone']]//following-sibling::input[@type='tel']")
	private WebElement phone;

	@FindBy(xpath = ".//label[span[text()='Fax']]//following-sibling::input[@type='tel']")
	private WebElement fax;

	@FindBy(xpath = ".//*[@label='Sold-To Party']")
	private WebElement rolesMultiselect;

	@FindBy(xpath = ".//*[@type='url']")
	private WebElement website;

	@FindBy(xpath = ".//label[span[text()='AE Responsible']]//following-sibling::div//input")
	private WebElement aeResponsible;

	@FindBy(xpath = ".//*[@label='Aromatics']")
	private WebElement productLineMultiSelect;

	@FindBy(xpath = ".//label[span[text()='CRC Responsible']]//following-sibling::div//input")
	private WebElement crcResponsible;

	@FindBy(xpath = ".//div[@class='uiInput uiInputTextArea uiInput--default uiInput--textarea']/textarea")
	private WebElement description;

	@FindBy(xpath = "//textarea[@placeholder='Street']")
	private WebElement street;

	@FindBy(xpath = "//input[@placeholder='City']")
	private WebElement city;

	@FindBy(xpath = "//input[@placeholder='State/Province']")
	private WebElement state;

	@FindBy(xpath = "//input[@placeholder='Zip/Postal Code']")
	private WebElement zip;

	@FindBy(xpath = "//input[@placeholder='Country']")
	private WebElement country;
	
	@FindBy(xpath = ".//*[@step='1']")
	private WebElement annualRevenue;
	
	@FindBy(xpath = ".//*[@class='select']")
	private WebElement customerSegment;

	public boolean enterAccountName(String AccountName) {
		Sync.waitForObject(driver, "Account Name", accountName);
		return Textbox.enterValue("Account Name", accountName, AccountName);
	}

	public boolean enterPhone(String Phone) {
		Sync.waitForObject(driver, "Phone Number", phone);
		return Textbox.enterValue("Phone Number", phone, Phone);
	}

	public boolean enterFax(String Fax) {
		Sync.waitForObject(driver, "Fax Number", fax);
		return Textbox.enterValue("Fax Number", fax, Fax);
	}

	public boolean enterRoles(String Roles) {
		Sync.waitForObject(driver, "Roles ", rolesMultiselect);
		return Link.click("Roles ", rolesMultiselect);
	}

	public boolean enterWebsite(String Website) {
		Sync.waitForObject(driver, "Website ", website);
		return Textbox.enterValue("Website ", website, Website);
	}


	public boolean enterAEResponsible(String AEResponsible) {
		Sync.waitForObject(driver, "AEResponsible", aeResponsible);
		return Textbox.enterAutocompleteValue("AEResponsible", aeResponsible, AEResponsible,driver);
	}
	public boolean enterProductLine(String ProductLine) {
		Sync.waitForObject(driver, "ProductLine", productLineMultiSelect);
		return Link.click("ProductLine", productLineMultiSelect);
	}

	public boolean enterCRCResponsible(String CRCResponsible) {
		Sync.waitForObject(driver, "CRCResponsible", crcResponsible);
		return Textbox.enterAutocompleteValue("CRCResponsible", crcResponsible, CRCResponsible, driver);
	}

	public boolean enterStreet(String Street) {
		Sync.waitForObject(driver, "Street", street);
		return Textbox.enterValue("Street", street, Street);
	}

	public boolean enterCity(String City) {
		Sync.waitForObject(driver, "City ", city);
		return Textbox.enterValue("City ", city, City);
	}

	public boolean enterState(String State) {
		Sync.waitForObject(driver, "State ", state);
		return Textbox.enterValue("Shipping State ", state, State);
	}

	public boolean enterZip(String Zip) {
		Sync.waitForObject(driver, "Zip ", zip);
		return Textbox.enterValue("Zip ", zip, Zip);
	}

	public boolean enterCountry(String Country) {
		Sync.waitForObject(driver, "Country ", country);
		return Textbox.enterValue("Country ", country, Country);
	}

	public boolean enterDescription(String Description) {
		Sync.waitForObject(driver, "Description", description);
		return Textbox.enterValue("Description", description, Description);
	}
	
	public boolean enterAnnualRevenue(String AnnualRevenue) {
		Sync.waitForObject(driver, "AnnualRevenue", annualRevenue);
		return Textbox.enterValue("AnnualRevenue", annualRevenue, AnnualRevenue);
	}
	
	public boolean selectCustomerSegment(String CustomerSegment) {
		Sync.waitForObject(driver, "Customer Segment", customerSegment);
		return Dropdown.selectByVisibleText("Customer Segment", customerSegment, CustomerSegment);
	}
}