package com.pages.crm360;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.common.toolbox.ErrorMessage;

public class OpportunityCommonErrorPage  extends CommonButtonPage {

	WebDriver driver;
	
	public OpportunityCommonErrorPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}
	
	
	// Error message while saving Opportunity without Mandatory fields.
		@FindBy(xpath = "//*[@class='message errorM3']/table/tbody/tr/td/img")
		WebElement errorMsgIcon;
	

	 public boolean errorForMandatoryFieldsOpportunty(String errorText) {
			return ErrorMessage.verifyErrorMessageOpportunity("Error Message", errorMsgIcon, errorText);
		}

	
		
}
