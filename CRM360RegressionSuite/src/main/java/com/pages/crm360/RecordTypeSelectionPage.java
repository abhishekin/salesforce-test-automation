package com.pages.crm360;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.common.misc.Sync;
import com.common.toolbox.Button;

public class RecordTypeSelectionPage {

	WebDriver driver;


	public RecordTypeSelectionPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}
	
	@FindBy(xpath = "//fieldset/div[2]/div[3]/label/div[1]/span")
	WebElement shiptoAccountRadio;
	
	@FindBy(xpath = "//fieldset/div[2]/div[1]/label/div[1]/span")
	WebElement soldtoAccountRadio;
	
	public boolean selectShipToRadio() {
		Sync.waitForObject(driver, "New Ship to Account", shiptoAccountRadio);
		return Button.click("New Ship to Account", shiptoAccountRadio);
	}
	
	public boolean selectSoldToRadio() {
		Sync.waitForObject(driver, "New Sold to Account", soldtoAccountRadio);
		return Button.click("New Sold to Account", soldtoAccountRadio);
	}
	
}
