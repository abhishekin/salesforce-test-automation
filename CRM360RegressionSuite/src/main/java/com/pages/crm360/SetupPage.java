package com.pages.crm360;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.common.misc.Sync;
import com.common.toolbox.Link;

public class SetupPage {

	WebDriver driver;

	@FindBy(xpath = "//li/a[text()='Users']")
	WebElement usersLink;

	@FindBy(xpath = "//li[3]/a[text() ='Profiles']")
	WebElement profilesLink;

	public SetupPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}

	public boolean clickUsersLink() {
		Sync.waitForPageLoad(driver);
		return Link.click("Users", usersLink);
	}

	public boolean clickProfilesLink() {
		Sync.waitForPageLoad(driver);
		return Link.click("Profiles", profilesLink);
	}

}
