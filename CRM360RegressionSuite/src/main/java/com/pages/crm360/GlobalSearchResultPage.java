package com.pages.crm360;

import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.common.misc.Sync;
import com.common.toolbox.Link;

public class GlobalSearchResultPage {

	WebDriver driver;

	public GlobalSearchResultPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}
	
	public boolean openAccount(String strValue) {
		Sync.waitForSeconds("WAIT_2");
		Sync.waitForFindElements(driver, "Account to Open", By.xpath(".//table/tbody/tr/th/span/a"));
		boolean status = false;
		List<WebElement> accountsNames = driver
				.findElements(By.xpath(".//table/tbody/tr/th/span/a"));
		Iterator<WebElement> itr = accountsNames.iterator();
		while (itr.hasNext()) {
			WebElement element = itr.next();
			if (element.getText().equals(strValue)) {
				status = Link.click("Open Account", element);
				break;
			}
		}

		return status;
	}

	public boolean openContact(String strValue) {
		Sync.waitForPageLoad(driver);
		boolean status = false;
		List<WebElement> names = driver.findElements(By.xpath("//div[@id='Contact_body']/table/tbody/tr[2]/th/a"));
		Iterator<WebElement> itr = names.iterator();
		while (itr.hasNext()) {
			WebElement element = itr.next();
			if (element.getText().equals(strValue)) {
				status = Link.click("Name", element);
				break;
			}
		}
		return status;
	}

	

	

	public boolean openActivitiesTaskEvent(String strValue) {
		Sync.waitForPageLoad(driver);
		boolean status = false;
		List<WebElement> openActivitiesTaskEvent = driver
				.findElements(By.xpath(".//div[contains(@class,taskBlock)]/div/div/table/tbody/tr/th/a"));
		Iterator<WebElement> itr = openActivitiesTaskEvent.iterator();
		while (itr.hasNext()) {
			WebElement element = itr.next();
			if (element.getText().equals(strValue)) {
				status = Link.click("openActivities(Task/Event)", element);
				break;
			}
		}
		return status;
	}

	public boolean checkNGSAPAccountTaskEventDelete(String strValue) {
		boolean status = true;
		List<WebElement> accountNames = driver
				.findElements(By.xpath(".//div[contains(@class,taskBlock)]/div/div/table/tbody/tr/th/a"));
		Iterator<WebElement> itr = accountNames.iterator();
		while (itr.hasNext()) {
			WebElement element = itr.next();
			if (element.getText().equals(strValue)) {
				status = false;
				System.out.println("Selected NGSAP account Task and Event was not deleted");
				return status;
			}
		}
		System.out.println("Selected NGSAP account Task and Event deleted successfully");
		return status;
	}

	public boolean openGenericOpportunity(String strValue) {
		boolean status = false;
		Sync.waitForFindElements(driver, "openGenericOpportunity", By.xpath(".//*[contains(@id,'_RelatedOpportunityList_body')]/table/tbody/tr/th/a"));
		List<WebElement> opportunitiesQA = driver
				.findElements(By.xpath(".//*[contains(@id,'_RelatedOpportunityList_body')]/table/tbody/tr/th/a"));
		Iterator<WebElement> itr = opportunitiesQA.iterator();
		while (itr.hasNext()) {
			WebElement element = itr.next();
			String s = element.getText();
			if (s.equals(strValue)) {
				status = Link.click("opportunities", element);
				break;
			}
		}
		return status;
	}

	public boolean openOpportunityCallPlan(String strValue) {
		boolean status = false;
		Sync.waitForFindElements(driver, "openOpportunityCallPlan", By.xpath(".//*[contains(@id , 'QA_body')]/table/tbody/tr/th/a"));
		List<WebElement> opportunitiesCallPlan = driver
				.findElements(By.xpath(".//*[contains(@id , 'QA_body')]/table/tbody/tr/th/a"));
		Iterator<WebElement> itr = opportunitiesCallPlan.iterator();
		while (itr.hasNext()) {
			WebElement element = itr.next();
			String s = element.getText();
			if (s.equals(strValue)) {
				status = Link.click("Opportunities CallPlan", element);
				break;
			}
		}
		return status;
	}

	public boolean checkNGSAPAccountOpportunityDelete(String strValue) {
		boolean status = true;
		List<WebElement> accountNames = driver.findElements(
				By.xpath(".//*[contains(@id ,'RelatedOpportunityList_body')]/table/tbody/tr/th/a"));
		Iterator<WebElement> itr = accountNames.iterator();
		while (itr.hasNext()) {
			WebElement element = itr.next();
			if (element.getText().equals(strValue)) {
				status = false;
				System.out.println("      Selected Opportunity was not deleted");
				return status;
			}
		}
		System.out.println("      Selected Opportunity deleted successfully");
		return status;
	}

	public boolean openNewNote(String strValue) {
		boolean status = false;
		List<WebElement> accountNote = driver
				.findElements(By.xpath(".//*[contains(@id ,'_RelatedNoteList_body')]/table/tbody/tr/td/a"));
		Iterator<WebElement> itr = accountNote.iterator();
		while (itr.hasNext()) {
			WebElement element = itr.next();
			if (element.getText().equals(strValue)) {
				status = Link.click("accountNotes", element);
				break;
			}
		}
		return status;
	}

	public boolean checkNoteDeletedAccount(String strValue) {
		boolean status = true;
		List<WebElement> accountNames = driver
				.findElements(By.xpath(".//*[contains(@id ,'_RelatedNoteList_body')]/table/tbody/tr/td[2]/a"));
		Iterator<WebElement> itr = accountNames.iterator();
		while (itr.hasNext()) {
			WebElement element = itr.next();
			if (element.getText().equals(strValue)) {
				status = false;
				System.out.println("Selected NGSAP account Note was not deleted");
				return status;
			}
		}
		System.out.println("Selected NGSAP account Note deleted successfully");
		return status;
	}

	public boolean accountEditTeamMember(String strValue) {
		boolean status = false;
		int i = 0;
		List<WebElement> accountNames = driver.findElements(
				By.xpath(".//div[contains(@class,'accountTeamMemberBlock')]/div/div/table/tbody/tr/th/a"));
		List<WebElement> accountLink = driver.findElements(
				By.xpath(".//div[contains(@class,'accountTeamMemberBlock')]/div/div/table/tbody/tr/td[1]/a[1]"));
		Iterator<WebElement> itr = accountNames.iterator();
		while (itr.hasNext()) {

			WebElement element = itr.next();
			if (element.getText().equals(strValue)) {
				WebElement aelement = accountLink.get(i);
				status = Link.click("accountTeamMembers", aelement);
				break;
			}
			i = i + 1;
		}
		return status;
	}

	public boolean accountDeleteTeamMember(String strValue) {
		boolean status = false;
		int i = 0;
		List<WebElement> accountNames = driver.findElements(
				By.xpath(".//div[contains(@class,'accountTeamMemberBlock')]/div/div/table/tbody/tr/th/a"));
		List<WebElement> accountLink = driver.findElements(
				By.xpath(".//div[contains(@class,'accountTeamMemberBlock')]/div/div/table/tbody/tr/td[1]/a[2]"));
		Iterator<WebElement> itr = accountNames.iterator();
		while (itr.hasNext()) {
			WebElement element = itr.next();
			if (element.getText().equals(strValue)) {
				WebElement aelement = accountLink.get(i);
				status = Link.click("accountTeamMembers", aelement);
				break;
			}
			i = i + 1;
		}
		return status;
	}

	public boolean checkTeamMemberDeletedAccount(String strValue) {
		boolean status = true;
		List<WebElement> accountNames = driver.findElements(
				By.xpath(".//div[contains(@class,'accountTeamMemberBlock')]/div/div/table/tbody/tr/th/a"));
		Iterator<WebElement> itr = accountNames.iterator();
		while (itr.hasNext()) {
			WebElement element = itr.next();
			if (element.getText().equals(strValue)) {
				status = false;
				System.out.println("Selected NGSAP account Team Member was not deleted");
				return status;
			}

		}
		System.out.println("Selected NGSAP account Team Member deleted successfully");
		return status;
	}

	public boolean openNewPorts(String strValue) {
		boolean status = false;
		List<WebElement> names = driver
				.findElements(By.xpath(".//*[contains(@id,'00Nb0000009P1gY_body')]/table/tbody/tr[2]/th/a"));
		Iterator<WebElement> itr = names.iterator();
		while (itr.hasNext()) {
			WebElement element = itr.next();
			if (element.getText().contains("PRT")) {
				status = Link.click("Open Port", element);
				break;
			}
		}
		return status;
	}
	
	public boolean openClonedPorts(String strValue) {
		boolean status = false;
		List<WebElement> names = driver
				.findElements(By.xpath(".//*[contains(@id,'00Nb0000009P1gY_body')]/table/tbody/tr[3]/th/a"));
		Iterator<WebElement> itr = names.iterator();
		while (itr.hasNext()) {
			WebElement element = itr.next();
			if (element.getText().contains("PRT")) {
				status = Link.click("Open Cloned Port", element);
				break;
			}
		}
		return status;
	}

	public boolean checkPortDeletedAccount(String strValue) {
		boolean status = true;
		List<WebElement> accountNames = driver
				.findElements(By.xpath("//*[contains(@id,'00Nb0000009P1gY_body')]/table/tbody/tr/th/a"));
		Iterator<WebElement> itr = accountNames.iterator();
		while (itr.hasNext()) {
			WebElement element = itr.next();
			if (element.getText().contains("PRT")) {
				status = false;
				System.out.println("      Selected NGSAP account Port was not deleted");
				return status;
			}
		}
		System.out.println("       Selected NGSAP account Port deleted successfully");
		return status;
	}

	public boolean openNGSAPAccountContact(String strValue) {
		boolean status = false;
		List<WebElement> names = driver.findElements(By.xpath("//*[contains(@id,'RelatedContactList_body')]/table/tbody/tr/th/a"));
		Iterator<WebElement> itr = names.iterator();
		while (itr.hasNext()) {
			WebElement element = itr.next();
			if (element.getText().equals(strValue)) {
				status = Link.click("Name", element);
				break;
			}
		}
		return status;
	}

	public boolean openAccountSalesAreaAssignment(String strValue) {
		Sync.waitForFindElements(driver, "SalesArea", By.xpath(".//*[contains(@id,'00Nb0000009P1Ke_body')]/table/tbody/tr[3]/th/a"));
		boolean status = false;
		List<WebElement> names = driver
				.findElements(By.xpath(".//*[contains(@id,'00Nb0000009P1Ke_body')]/table/tbody/tr[3]/th/a"));
		Iterator<WebElement> itr = names.iterator();
		while (itr.hasNext()) {
			WebElement element = itr.next();
			if (element.getText().contains("SAA")) {
				status = Link.click("Open Sales Area", element);
				break;
			}
		}
		return status;
	}
	
	public boolean openClonedAccountSalesAreaAssignment(String strValue) {
		Sync.waitForFindElements(driver, "SalesArea", By.xpath(".//*[contains(@id,'00Nb0000009P1Ke_body')]/table/tbody/tr[4]/th/a"));
		boolean status = false;
		List<WebElement> names = driver
				.findElements(By.xpath(".//*[contains(@id,'00Nb0000009P1Ke_body')]/table/tbody/tr[4]/th/a"));
		Iterator<WebElement> itr = names.iterator();
		while (itr.hasNext()) {
			WebElement element = itr.next();
			if (element.getText().contains("SAA")) {
				status = Link.click("Open Cloned Sales Area", element);
				break;
			}
		}
		return status;
	}
	
	/*public boolean openAccountSalesAreaAssignment(String strValue) {
		Boolean status = false;
		List<WebElement> rows = driver
				.findElements(By.xpath(".//*[contains(@id,'00Nb0000009P1Ke_body')]/table/tbody/tr[contains(@class,'dataRow')]"));
		for (WebElement row : rows) {
			WebElement salesAreaName = row.findElement(
					By.xpath(".//*[contains(@id,'00Nb0000009P1Ke_body')]/table/tbody/tr/td[2]"));
			String s = salesAreaName.getText();
			System.out.println("Sales area Name "+s);
			if (s.equals(strValue)) {
				WebElement salesAreaId = row.findElement(By.xpath(".//*[contains(@id,'00Nb0000009P1Ke_body')]/table/tbody/tr/th/a"));
						String s1 = salesAreaId.getText();	
						System.out.println("salesAreaId"+s1);
				status = Link.click("Open Sales Area Assignment", salesAreaId);
				break;
			}
		}
		return status;
		
	}*/
	/*public boolean deleteAccountSalesAreaAssignment(String strValue) {
			Boolean status = false;
			List<WebElement> rows = driver
					.findElements(By.xpath(".//*[contains(@id,'00Nb0000009P1Ke_body')]/table/tbody/tr[contains(@class,'dataRow')]"));
			for (WebElement row : rows) {
				WebElement salesAreaName = row.findElement(
						By.xpath("//*[contains(@id,'00Nb0000009P1Ke_body')]/table/tbody/tr/td[2]"));
				String s = salesAreaName.getText();
				System.out.println("Sales area Name "+s);
				if (s.equals(strValue)) {
					WebElement delete = row.findElement(By.xpath(".//*[contains(@id,'00Nb0000009P1Ke_body')]/table/tbody/tr/td/a[2]"));
							String s1 = delete.getText();	
							System.out.println("salesAreaId"+s1);
					status = Link.click("Delete Sales Area Assignment", delete);
					break;
				}
			}
			return status;
	}*/
	
	
	public boolean deleteFirstAccountSalesAreaAssignment(String strValue) {
		boolean status = false;
		Sync.waitForFindElements(driver, "SalesArea", By.xpath(".//*[contains(@id,'00Nb0000009P1Ke_body')]/table/tbody/tr[2]/td[1]/a[2]"));
		List<WebElement> names = driver
				.findElements(By.xpath(".//*[contains(@id,'00Nb0000009P1Ke_body')]/table/tbody/tr[2]/td[1]/a[2]"));
		Iterator<WebElement> itr = names.iterator();
		while (itr.hasNext()) {
			WebElement element = itr.next();
			if (element.getText().contains("Del")) {
				status = Link.click("Delete SalesArea", element);
				break;
			}
		}
		return status;
	}
	
	public boolean deleteAccountSalesAreaAssignment(String strValue) {
		boolean status = false;
		List<WebElement> names = driver
				.findElements(By.xpath(".//*[contains(@id,'00Nb0000009P1Ke_body')]/table/tbody/tr[3]/td[1]/a[2]"));
		Iterator<WebElement> itr = names.iterator();
		while (itr.hasNext()) {
			WebElement element = itr.next();
			if (element.getText().contains("Del")) {
				status = Link.click("Delete SalesArea", element);
				break;
			}
		}
		return status;
	}
		public boolean deleteClonedAccountSalesAreaAssignment(String strValue) {
			boolean status = false;
			List<WebElement> names = driver
					.findElements(By.xpath(".//*[contains(@id,'00Nb0000009P1Ke_body')]/table/tbody/tr[4]/td[1]/a[2]"));
			Iterator<WebElement> itr = names.iterator();
			while (itr.hasNext()) {
				WebElement element = itr.next();
				if (element.getText().contains("Del")) {
					status = Link.click("Delete Cloned SalesArea", element);
					break;
				}
			}
			return status;
		
	}
	public boolean openLead(String strValue) {
		boolean status = false;
		List<WebElement> Leads = driver.findElements(By.xpath("//*[@id='Lead_body']/table/tbody/tr[2]/th/a"));
		Iterator<WebElement> itr = Leads.iterator();
		while (itr.hasNext()) {
			WebElement element = itr.next();
			if (element.getText().equals(strValue)) {
				status = Link.click("Lead", element);
				break;
			}
		}
		return status;
	}

	public boolean openAccountDelete(String strValue) {
		boolean status = false;
		int i = 0;
		List<WebElement> accountNames = driver
				.findElements(By.xpath(".//*[contains(@id,'ACCOUNT_NAME')]/a/span"));
		List<WebElement> accountDeleteLink = driver
				.findElements(By.xpath(".//*[contains(@id,'ACTION_COLUMN')]/a[2]/span"));
		Iterator<WebElement> itr = accountNames.iterator();
		while (itr.hasNext()) {
			WebElement element = itr.next();
			if (element.getText().equals(strValue)) {
				WebElement aelement = accountDeleteLink.get(i);
				status = Link.click("Account To Delete", aelement);
				break;
			}
			i = i + 1;
		}
		return status;
	}

	public boolean checkAccountDelete(String strValue) {
		boolean status = true;
		Sync.waitForObject(driver, "Account Names", By.id("ext-gen29"));
		List<WebElement> accountNames = driver
				.findElements(By.xpath(".//*[contains(@id,'ACCOUNT_NAME')]/a/span"));
		Iterator<WebElement> itr = accountNames.iterator();
		while (itr.hasNext()) {
			WebElement element = itr.next();
			if (element.getText().equals(strValue)) {
				status = false;
				System.out.println("     - Selected account was not deleted");
				return status;
			}
		}
		System.out.println("     - Selected account deleted successfully");
		return status;
	}

	public boolean openNGSAPAccountPlan(String strValue) {
		Sync.waitForFindElements(driver, "PlanName", By.xpath(".//*[contains(@id,'Nb0000009P1KZ_body')]/table/tbody/tr/th/a"));
		boolean status = false;
		List<WebElement> names = driver
				.findElements(By.xpath(".//*[contains(@id,'Nb0000009P1KZ_body')]/table/tbody/tr/th/a"));
		Iterator<WebElement> itr = names.iterator();
		while (itr.hasNext()) {
			WebElement element = itr.next();
			String s = element.getText();
			if (s.equals(strValue)) {
				status = Link.click("Account Plan is Opened", element);
				break;
			}
		}

		return status;
	}

	public boolean checkNGSAPAccountPlanDelete(String strValue) {
		boolean status = true;
		List<WebElement> accountNames = driver
				.findElements(By.xpath(".//*[contains(@id,'Nb0000009P1KZ_body')]/table/tbody/tr/th/a"));
		Iterator<WebElement> itr = accountNames.iterator();
		while (itr.hasNext()) {
			WebElement element = itr.next();
			if (element.getText().equals(strValue)) {
				status = false;
				System.out.println("      Selected NGSAP account Plan was not deleted");
				return status;
			}
		}
		System.out.println("      Selected NGSAP account Plan deleted successfully");
		return status;
	}

	public boolean openNGSAPAccountCallPlan(String strValue) {
		Sync.waitForFindElements(driver, "CallPlans", By.xpath("//*[contains(@id,'00Nb0000009P1Ka_body')]/table/tbody/tr/th/a"));
		boolean status = false;
		List<WebElement> CallplanNames = driver
				.findElements(By.xpath("//*[contains(@id,'00Nb0000009P1Ka_body')]/table/tbody/tr/th/a"));
		Iterator<WebElement> itr = CallplanNames.iterator();
		while (itr.hasNext()) {
			WebElement element = itr.next();
			if (element.getText().equals(strValue)) {
				status = Link.click("CallPlanNames", element);
				break;
			}
		}
		return status;
	}

	public boolean openOpportunityTeamMember(String strValue) {
		boolean status = false;
		List<WebElement> TeamMembers = driver
				.findElements(By.xpath(".//*[contains(@id,'RelatedOpportunitySalesTeam_body')]/table/tbody/tr/th/a"));
		Iterator<WebElement> itr = TeamMembers.iterator();
		while (itr.hasNext()) {
			WebElement element = itr.next();
			if (element.getText().equals(strValue)) {
				status = Link.click("Team Members", element);
				break;
			}
		}
		return status;
	}

	public boolean deleteOpportunityTeamMember(String strValue) {
		Boolean status = false;
		List<WebElement> rows = driver
				.findElements(By.xpath("//*[contains(@id,'RelatedOpportunitySalesTeam_body')]/table/tbody/tr"));
		for (WebElement row : rows) {
			WebElement competitor = row
					.findElement(By.xpath("//*[contains(@id,'RelatedOpportunitySalesTeam_body')]/table/tbody/tr/th/a"));
			String s = competitor.getText();
			if (s.equals(strValue)) {
				WebElement delete = row.findElement(By.xpath(
						"//*[contains(@id,'RelatedOpportunitySalesTeam_body')]/table/tbody/tr/td/a[contains(@title,'Delete')]"));
				status = Link.click("Delete Team Member", delete);
				break;
			}
		}
		return status;
	}

	public boolean checkOpportunityTeamMemberDeleted(String strValue) {
		boolean status = true;
		List<WebElement> accountNames = driver
				.findElements(By.xpath(".//*[contains(@id,'RelatedOpportunitySalesTeam_body')]/table/tbody/tr/th/a"));
		Iterator<WebElement> itr = accountNames.iterator();
		while (itr.hasNext()) {
			WebElement element = itr.next();
			if (element.getText().equals(strValue)) {
				status = false;
				System.out.println("Selected Oppty Team Member was not deleted");
				return status;
			}
		}
		System.out.println("Selected Oppty Team Member deleted successfully");
		return status;
	}

	public boolean openCompetitor(String strValue) {
		boolean status = false;
		List<WebElement> compNote = driver
				.findElements(By.xpath("//*[contains(@id,'RelatedCompetitorList_body')]/table/tbody/tr/th/a"));
		Iterator<WebElement> itr = compNote.iterator();
		while (itr.hasNext()) {
			WebElement element = itr.next();
			if (element.getText().equals(strValue)) {
				status = Link.click("Competitor", element);
				break;
			}
		}
		return status;
	}

	public boolean opptyCompetitorDelete(String strValue) {
		Boolean status = false;
		List<WebElement> rows = driver
				.findElements(By.xpath("//*[contains(@id,'RelatedCompetitorList_body')]/table/tbody/tr"));
		for (WebElement row : rows) {
			WebElement competitor = row
					.findElement(By.xpath("//*[contains(@id,'RelatedCompetitorList_body')]/table/tbody/tr/th/a"));
			String s = competitor.getText();
			if (s.equals(strValue)) {
				WebElement delete = row.findElement(By.xpath(
						"//*[contains(@id,'RelatedCompetitorList_body')]/table/tbody/tr/td/a[contains(@title,'Delete')]"));
				status = Link.click("Delete Competitor", delete);
				break;
			}
		}
		return status;
	}

	public boolean openOpportunityContact(String strValue) {
		boolean status = false;
		List<WebElement> names = driver
				.findElements(By.xpath("//*[contains(@id,'RelatedContactRoleList_body')]/table/tbody/tr/th/a"));
		Iterator<WebElement> itr = names.iterator();
		while (itr.hasNext()) {
			WebElement element = itr.next();
			if (element.getText().equals(strValue)) {
				status = Link.click("Contact Name", element);
				break;
			}
		}
		return status;
	}

	public boolean opptyContactRecordDelete(String strValue) {
		Boolean status = false;
		List<WebElement> rows = driver
				.findElements(By.xpath("//*[contains(@id,'RelatedContactRoleList_body')]/table/tbody/tr"));
		for (WebElement row : rows) {
			WebElement product = row
					.findElement(By.xpath("//*[contains(@id,'RelatedContactRoleList_body')]/table/tbody/tr/th/a"));
			String s = product.getText();
			if (s.equals(strValue)) {
				WebElement delete = row.findElement(By.xpath(
						"//*[contains(@id,'RelatedContactRoleList_body')]/table/tbody/tr/td/a[contains(@title,'Delete')]"));
				status = Link.click("Delete Contact Record", delete);
				break;
			}
		}
		return status;
	}

	public boolean openCampaign(String strValue) {
		boolean status = false;
		List<WebElement> campaignPage = driver.findElements(
				By.xpath("//*[contains(@id,'RelatedCampaignInfluenceList_body')]/table/tbody/tr[2]/th/a"));
		Iterator<WebElement> itr = campaignPage.iterator();
		while (itr.hasNext()) {
			WebElement element = itr.next();
			if (element.getText().equals(strValue)) {
				status = Link.click("Open Campaign", element);
				break;
			}
		}
		return status;
	}

	public boolean opptyCampaignRecordDelete(String strValue) {
		Boolean status = false;
		List<WebElement> rows = driver
				.findElements(By.xpath("//*[contains(@id,'RelatedCampaignInfluenceList_body')]/table/tbody/tr"));
		for (WebElement row : rows) {
			WebElement product = row.findElement(
					By.xpath("//*[contains(@id,'RelatedCampaignInfluenceList_body')]/table/tbody/tr/th/a"));
			String s = product.getText();
			if (s.equals(strValue)) {
				WebElement delete = row.findElement(By.xpath(
						"//*[contains(@id,'RelatedCampaignInfluenceList_body')]/table/tbody/tr/td/a[contains(@title,'Delete')]"));
				status = Link.click("Delete Contact Record", delete);
				break;
			}
		}
		return status;
	}
	
	public boolean openNewContract() {
		boolean status = false;
		List<WebElement> names = driver
				.findElements(By.xpath(".//*[contains(@id,'RelatedContractList_body')]/table/tbody/tr[2]/th/a"));
		Iterator<WebElement> itr = names.iterator();
		while (itr.hasNext()) {
			WebElement element = itr.next();
			if (element.getText().contains("0")) {
				status = Link.click("Open Contract", element);
				break;
			}
		}
		return status;
	}
	
	public boolean openCloneContract() {
		boolean status = false;
		List<WebElement> names = driver
				.findElements(By.xpath(".//*[contains(@id,'RelatedContractList_body')]/table/tbody/tr[3]/th/a"));
		Iterator<WebElement> itr = names.iterator();
		while (itr.hasNext()) {
			WebElement element = itr.next();
			if (element.getText().contains("0")) {
				status = Link.click("Open Cloned Contract", element);
				break;
			}
		}
		return status;
	}
}