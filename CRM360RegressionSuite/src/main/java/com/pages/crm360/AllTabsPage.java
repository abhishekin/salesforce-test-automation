package com.pages.crm360;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.common.misc.Sync;
import com.common.toolbox.Link;

public class AllTabsPage {
	

	WebDriver driver;
    
    @FindBy(xpath=".//span[@class='slds-truncate'][text() ='Contacts']")
    WebElement contactsLink;
    
	@FindBy(xpath = ".//span[@class='slds-truncate'][text() ='Accounts']")
	WebElement accountTab;
	
	@FindBy(xpath = ".//span[@class='slds-truncate'][text() ='Leads']")
	WebElement leadsTab;
	

    public AllTabsPage(WebDriver driver){
    	PageFactory.initElements(driver, this);
        this.driver = driver;
    }

   
    public boolean clickContactsTab()
    {
    	Sync.waitForObject(driver, "Contacts", contactsLink);
    	return Link.click("Contacts", contactsLink);
    }
    
	public boolean clickAccountTab() {
		Sync.waitForObject(driver, "Account Tab", accountTab);
		return Link.click("Account Tab", accountTab);
	}
	public boolean clickLeadsTab() {
		Sync.waitForObject(driver, "Leads Tab", leadsTab);
		return Link.click("Leads Tab", leadsTab);
	}
}
