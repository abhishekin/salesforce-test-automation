package com.pages.crm360;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.common.misc.Sync;
import com.common.toolbox.Link;
import com.common.toolbox.Textbox;

/**
 * Created by AbhishekTiwari on 28/Aug/2017.
 */

public class AccountShipToCreatePage {

	WebDriver driver;

	public AccountShipToCreatePage(WebDriver driver) {
		// super(driver);
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}

	@FindBy(xpath = ".//*[@class='input uiInput uiInputText uiInput--default uiInput--input']")
	private WebElement accountName;

	@FindBy(xpath = ".//label[span[text()='Phone']]//following-sibling::input[@type='tel']")
	private WebElement phone;

	@FindBy(xpath = ".//label[span[text()='Fax']]//following-sibling::input[@type='tel']")
	private WebElement fax;

	@FindBy(xpath = ".//*[@label='Sold-To Party']")
	private WebElement rolesMultiselect;

	@FindBy(xpath = ".//*[@type='url']")
	private WebElement website;

	@FindBy(xpath = "//input[@placeholder='Search Accounts']")
	private WebElement parentAccount;

	@FindBy(xpath = ".//label[span[text()='AE Responsible']]//following-sibling::div//input")
	private WebElement aeResponsible;

	@FindBy(xpath = ".//*[@label='Aromatics']")
	private WebElement productLineMultiSelect;

	@FindBy(xpath = ".//label[span[text()='CRC Responsible']]//following-sibling::div//input")
	private WebElement crcResponsible;

	@FindBy(xpath = ".//div[@class='uiInput uiInputTextArea uiInput--default uiInput--textarea']/textarea")
	private WebElement description;

	@FindBy(xpath = "//textarea[@placeholder='Shipping Street']")
	private WebElement shippingStreet;

	@FindBy(xpath = "//input[@placeholder='Shipping City']")
	private WebElement shippingCity;

	@FindBy(xpath = "//input[@placeholder='Shipping State/Province']")
	private WebElement shippingState;

	@FindBy(xpath = "//input[@placeholder='Shipping Zip/Postal Code']")
	private WebElement shippingZip;

	@FindBy(xpath = "//input[@placeholder='Shipping Country']")
	private WebElement shippingCountry;

	public boolean enterAccountName(String AccountName) {
		Sync.waitForObject(driver, "Account Name", accountName);
		return Textbox.enterValue("Account Name", accountName, AccountName);
	}

	public boolean enterPhone(String Phone) {
		Sync.waitForObject(driver, "Phone Number", phone);
		return Textbox.enterValue("Phone Number", phone, Phone);
	}

	public boolean enterFax(String Fax) {
		Sync.waitForObject(driver, "Fax Number", fax);
		return Textbox.enterValue("Fax Number", fax, Fax);
	}

	public boolean enterRoles(String Roles) {
		Sync.waitForObject(driver, "Roles ", rolesMultiselect);
		return Link.click("Roles ", rolesMultiselect);
	}

	public boolean enterWebsite(String Website) {
		Sync.waitForObject(driver, "Website ", website);
		return Textbox.enterValue("Website ", website, Website);
	}

	public boolean enterParentAccount(String ParentAccount) {
		Sync.waitForObject(driver, "Parent Account", parentAccount);
		return Textbox.enterAutocompleteValue("Parent Account", parentAccount, ParentAccount, driver);
	}

	public boolean enterAEResponsible(String AEResponsible) {
		Sync.waitForObject(driver, "AEResponsible", aeResponsible);
		return Textbox.enterAutocompleteValue("AEResponsible", aeResponsible, AEResponsible,driver);
	}
	public boolean enterProductLine(String ProductLine) {
		Sync.waitForObject(driver, "ProductLine", productLineMultiSelect);
		return Link.click("ProductLine", productLineMultiSelect);
	}

	public boolean enterCRCResponsible(String CRCResponsible) {
		Sync.waitForObject(driver, "CRCResponsible", crcResponsible);
		return Textbox.enterAutocompleteValue("CRCResponsible", crcResponsible, CRCResponsible, driver);
	}

	public boolean enterShippingStreet(String ShippingStreet) {
		Sync.waitForObject(driver, "Shipping Street", shippingStreet);
		return Textbox.enterValue("Shipping Street", shippingStreet, ShippingStreet);
	}

	public boolean enterShippingCity(String ShippingCity) {
		Sync.waitForObject(driver, "Shipping City ", shippingCity);
		return Textbox.enterValue("Shipping City ", shippingCity, ShippingCity);
	}

	public boolean enterShippingState(String ShippingState) {
		Sync.waitForObject(driver, "Shipping State ", shippingState);
		return Textbox.enterValue("Shipping State ", shippingState, ShippingState);
	}

	public boolean enterShippingZip(String ShippingZip) {
		Sync.waitForObject(driver, "Shipping Zip ", shippingZip);
		return Textbox.enterValue("Shipping Zip ", shippingZip, ShippingZip);
	}

	public boolean enterShippingCountry(String ShippingCountry) {
		Sync.waitForObject(driver, "Shipping Country ", shippingCountry);
		return Textbox.enterValue("Shipping Country ", shippingCountry, ShippingCountry);
	}

	public boolean enterDescription(String Description) {
		Sync.waitForObject(driver, "Description", description);
		return Textbox.enterValue("Description", description, Description);
	}
}