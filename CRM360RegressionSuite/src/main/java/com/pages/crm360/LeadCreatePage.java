package com.pages.crm360;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.common.misc.Sync;
import com.common.toolbox.Dropdown;
import com.common.toolbox.Textbox;

/**
 * Created by Ashmita Bhattacharyya on 31/Aug/2017.
 */

public class LeadCreatePage {

	WebDriver driver;

	public LeadCreatePage(WebDriver driver) {
		// super(driver);
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}

	@FindBy(xpath = ".//*[@class='compoundTLRadius compoundTRRadius compoundBorderBottom form-element__row uiMenu']")
	private WebElement salutation;

	@FindBy(xpath = ".//*[@placeholder='First Name']")
	private WebElement firstName;

	@FindBy(xpath = ".//*[@placeholder='Middle Name']")
	private WebElement middleName;
	
	@FindBy(xpath = ".//*[@placeholder='Last Name']")
	private WebElement lastName;

	@FindBy(xpath = ".//*[@placeholder='Suffix']")
	private WebElement suffix;
	
	@FindBy(xpath = ".//label[span[text()='Title']]//following-sibling::input[@type='text']")
	private WebElement title;
	
	@FindBy(xpath = ".//a[@aria-label='Rating']")
	private WebElement rating;
	
	@FindBy(xpath = ".//label[span[text()='Company']]//following-sibling::input[@type='text']")
	private WebElement company;
	
	@FindBy(xpath = ".//a[@aria-label='Preferred method of contact']")
	private WebElement preferredMethodOfContact;
	
	@FindBy(xpath = ".//label[span[text()='Annual Revenue']]//following-sibling::input[@type='text']")
	private WebElement annualRevenue;
	
	@FindBy(xpath = ".//label[span[text()='Office Phone']]//following-sibling::input[@type='tel']")
	private WebElement officePhone;
	
	@FindBy(xpath = ".//a[@aria-label='Lead Source']")
	private WebElement leadSource;
	
	@FindBy(xpath = ".//label[span[text()='Mobile']]//following-sibling::input[@type='tel']")
	private WebElement mobile;
	
	@FindBy(xpath = ".//label[span[text()='Product Line']]//following-sibling::select")
	private WebElement productLine;
	
	@FindBy(xpath = ".//label[span[text()='Fax']]//following-sibling::input[@type='tel']")
	private WebElement fax;
	
	@FindBy(xpath = ".//a[@aria-label='Sales Region']")
	private WebElement salesRegion;
	
	@FindBy(xpath = ".//label[span[text()='Email']]//following-sibling::input[@type='email']")
	private WebElement email;
	
	@FindBy(xpath = ".//label[span[text()='Description']]//following-sibling::textarea")
	private WebElement description;
	
	@FindBy(xpath = ".//label[span[text()='Website']]//following-sibling::input[@type='url']")
	private WebElement website;
	
	@FindBy(xpath = ".//button[span[@class='searchLabel']]")
	private WebElement searchAddress;
	
	@FindBy(xpath = ".//label[span[text()='Street']]//following-sibling::textarea")
	private WebElement street;
	
	@FindBy(xpath = ".//label[span[text()='City']]//following-sibling::input[@type='text']")
	private WebElement city;
	
	@FindBy(xpath = ".//label[span[text()='State/Province']]//following-sibling::input[@type='text']")
	private WebElement stateProvince;
	
	@FindBy(xpath = ".//label[span[text()='ZIP']]//following-sibling::input[@type='text']")
	private WebElement zip;
	
	@FindBy(xpath = ".//label[span[text()='Country']]//following-sibling::input[@type='text']")
	private WebElement country;
	
	@FindBy(xpath = ".//a[@aria-label='Lead Status']")
	private WebElement leadStatus;
	
	
	public boolean selectSalutation(String Salutation){
		Sync.waitForObject(driver, "Salutation", salutation);
		return Dropdown.selectByVisibleText("Salutation", salutation, Salutation);
	}
	
	public boolean enterFirstName(String FirstName) {
		Sync.waitForObject(driver, "First Name", firstName);
		return Textbox.enterValue("First Name", firstName, FirstName);
	}

	public boolean enterMiddleName(String MiddleName) {
		Sync.waitForObject(driver, "Middle Name", middleName);
		return Textbox.enterValue("Middle Name", middleName, MiddleName);
	}
	
	public boolean enterLastName(String LastName) {
		Sync.waitForObject(driver, "Last Name", lastName);
		return Textbox.enterValue("Last Name", lastName, LastName);
	}	
	
	public boolean enterSuffix(String Suffix) {
		Sync.waitForObject(driver, "Suffix", suffix);
		return Textbox.enterValue("Suffix", suffix, Suffix);
	}
	
	public boolean enterTitle(String Title) {
		Sync.waitForObject(driver, "Title", title);
		return Textbox.enterValue("Title", title, Title);
	}
	
	public boolean enterCompany(String Company) {
		Sync.waitForObject(driver, "Company", company);
		return Textbox.enterValue("Company", company, Company);
	}
	
	public boolean enterAnnualRevenue(String AnnualRevenue) {
		Sync.waitForObject(driver, "AnnualRevenue", annualRevenue);
		return Textbox.enterValue("AnnualRevenue", annualRevenue, AnnualRevenue);
	}
	
	public boolean selectLeadSource(String LeadSource){
		Sync.waitForObject(driver, "LeadSource", leadSource);
		return Dropdown.selectByVisibleText("LeadSource", leadSource, LeadSource);
	}
	
	//public boolean selectProductLine() --Need method for multi-select
	
	public boolean selectSalesRegion(String SalesRegion){
		Sync.waitForObject(driver, "Sales Region", salesRegion);
		return Dropdown.selectByVisibleText("Sales Region", salesRegion, SalesRegion);
	}
	
	public boolean enterDescription(String Description) {
		Sync.waitForObject(driver, "Description", description);
		return Textbox.enterValue("Description", description, Description);
	}
	
	public boolean selectRating(String Rating){
		Sync.waitForObject(driver, "Rating", rating);
		return Dropdown.selectByVisibleText("Rating", rating, Rating);
	}
	
	public boolean selectPreferredMethodOfContact(String PreferredMethodOfContact){
		Sync.waitForObject(driver, "Preferred Method Of Contact", preferredMethodOfContact);
		return Dropdown.selectByVisibleText("Preferred Method Of Contact", preferredMethodOfContact, PreferredMethodOfContact);
	}
	
	public boolean enterOfficePhone(String OfficePhone) {
		Sync.waitForObject(driver, "Office Phone", officePhone);
		return Textbox.enterValue("Office Phone", officePhone, OfficePhone);
	}
	
	public boolean enterMobile(String Mobile) {
		Sync.waitForObject(driver, "Mobile", mobile);
		return Textbox.enterValue("Mobile", mobile, Mobile);
	}	
	
	public boolean enterFax(String Fax) {
		Sync.waitForObject(driver, "Fax", fax);
		return Textbox.enterValue("Fax", fax, Fax);
	}	
	
	public boolean enterEmail(String Email) {
		Sync.waitForObject(driver, "Email", email);
		return Textbox.enterValue("Email", email, Email);
	}	
	
	public boolean enterWebsite(String Website) {
		Sync.waitForObject(driver, "Website", website);
		return Textbox.enterValue("Website", website, Website);
	}	

	public boolean enterStreet(String Street) {
		Sync.waitForObject(driver, "Street", street);
		return Textbox.enterValue("Street", street, Street);
	}

	public boolean enterCity(String City) {
		Sync.waitForObject(driver, "City ", city);
		return Textbox.enterValue("City ", city, City);
	}

	public boolean enterStateProvince(String StateProvince) {
		Sync.waitForObject(driver, "State/Province", stateProvince);
		return Textbox.enterValue("State/Province", stateProvince, StateProvince);
	}

	public boolean enterZip(String Zip) {
		Sync.waitForObject(driver, "ZIP ", zip);
		return Textbox.enterValue("ZIP ", zip, Zip);
	}

	public boolean enterCountry(String Country) {
		Sync.waitForObject(driver, "Country ", country);
		return Textbox.enterValue("Country ", country, Country);
	}
	
	public boolean selectLeadStatus(String LeadStatus){
		Sync.waitForObject(driver, "Lead Status", leadStatus);
		return Dropdown.selectByVisibleText("Preferred Method Of Contact", leadStatus, LeadStatus);
	}

}