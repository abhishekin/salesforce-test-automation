package com.pages.crm360;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.common.toolbox.Textbox;

public class AccountSoldToDetailPage {

	WebDriver driver;

	public AccountSoldToDetailPage(WebDriver driver) {
		// super(driver);
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}
	// Sold To

	@FindBy(xpath = "//div/div[1]/div[1]/div/div[2]/span/span[@class='uiOutputText']")
	private WebElement accountName;

	@FindBy(xpath = ".//div/div[1]/div[2]/div/div[2]/span/span[@class='uiOutputPhone']")
	private WebElement phone;

	@FindBy(xpath = "//div/div[2]/div[2]/div/div[2]/span/span[@class='uiOutputPhone']")
	private WebElement fax;

	@FindBy(xpath = "//div/div[2]/div[1]/div/div[2]/span/div/div/span")
	private WebElement accountRecordType;

	@FindBy(xpath = ".//div/div[3]/div[1]/div/div[2]/span")
	private WebElement roles;

	@FindBy(xpath = "//div/div[3]/div[2]/div/div[2]/span")
	private WebElement website;

	@FindBy(xpath = "//div/div[4]/div[2]/div/div[2]/span/div/a")
	private WebElement aeResponsible;

	@FindBy(xpath = "//div/div[4]/div[1]/div/div[2]/span")
	private WebElement productLine;

	@FindBy(xpath = "//div/div[5]/div[2]/div/div[2]/span/div/a")
	private WebElement crcResponsible;

	@FindBy(xpath = "//span[@class='uiOutputTextArea']")
	private WebElement description;

	@FindBy(xpath = "//div/div/div[1]/div/div[2]/span/a/div[1]")
	private WebElement street;

	@FindBy(xpath = "//div/div/div[1]/div/div[2]/span/a/div[2]/span[1]")
	private WebElement city;

	@FindBy(xpath = "//div/div/div[1]/div/div[2]/span/a/div[2]/span[3]")
	private WebElement state;

	@FindBy(xpath = "//div/div/div[1]/div/div[2]/span/a/div[2]/span[4]")
	private WebElement zip;

	@FindBy(xpath = "//div/div/div[1]/div/div[2]/span/a/div[2]/span[5]")
	private WebElement country;

	@FindBy(xpath = "//div/div[1]/div[2]/div/div[2]/span/span[@class='forceOutputCurrency']")
	private WebElement annualRevenue;

	@FindBy(xpath = ".//*[@style='transition: none 0s ease 0s ;']/div/div[2]/div[2]/div/div[2]/span/span")
	private WebElement segment;
	
	

	public boolean verifyAccountName(String AccountName) {
		return Textbox.verifyText("Description/Name", accountName, AccountName);
	}

	public boolean verifyPhone(String Phone) {
		return Textbox.verifyText("Phone", phone, Phone);
	}

	public boolean verifyFax(String Fax) {
		return Textbox.verifyText("Fax", fax, Fax);
	}

	public boolean verifyAccountRecordType(String AccountRecordType) {
		return Textbox.verifyText("AccountRecordType", accountRecordType, AccountRecordType);
	}

	public boolean verifyRoles(String Roles) {
		return Textbox.verifyText("Roles", roles, Roles);
	}

	public boolean verifyWebsite(String Website) {
		return Textbox.verifyText("WebSites", website, Website);
	}

	public boolean verifyAEResponsible(String AEResponsible) {
		return Textbox.verifyText("AE Responsible", aeResponsible, AEResponsible);
	}

	public boolean verifyProductLine(String ProductLine) {
		return Textbox.verifyText("Product Line", productLine, ProductLine);
	}

	public boolean verifyCRCResponsible(String CRCResponsible) {
		return Textbox.verifyText("CRCResponsible", crcResponsible, CRCResponsible);
	}

	public boolean verifyStreet(String Street) {
		return Textbox.verifyText("Street", street, Street);
	}

	public boolean verifyCity(String City) {
		return Textbox.verifyText("City", city, City);
	}

	public boolean verifyState(String State) {
		return Textbox.verifyText("State", state, State);
	}

	public boolean verifyZip(String Zip) {
		return Textbox.verifyText("Zip", zip, Zip);
	}

	public boolean verifyCountry(String Country) {
		return Textbox.verifyText("Country", country, Country);
	}

	public boolean verifyDescription(String Description) {
		return Textbox.verifyText("description", description, Description);
	}

	public boolean verifyAnnualRevenue(String AnnualRevenue) {
		return Textbox.verifyText("AnnualRevenue", annualRevenue, AnnualRevenue);
	}

	public boolean verifySegment(String Segment) {
		return Textbox.verifyText("Segment", segment, Segment);
	}

}
