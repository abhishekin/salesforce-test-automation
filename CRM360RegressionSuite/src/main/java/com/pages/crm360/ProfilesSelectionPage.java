package com.pages.crm360;

import java.awt.AWTException;
import java.awt.Robot;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.common.misc.Sync;
import com.common.toolbox.Button;
import com.common.toolbox.Frames;
import com.common.toolbox.Link;

public class ProfilesSelectionPage {

	WebDriver driver;


	public ProfilesSelectionPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}
	
	@FindBy(xpath = ".//*[@title='View Users']")
	WebElement viewUsersBtn;

	@FindBy(xpath = ".//*[@class='pageDescription']")
	WebElement hoverElement;
	
	

	public boolean selectAlphabetIndex(String profileName) {
		Sync.waitForPageLoad(driver);
		Frames.switchToFrame(driver, "//iframe[contains(@id,'vfFrameId_')]");
		Sync.waitForObject(driver, "Alphabet To Select", By.xpath(".//*[contains(@id,'_rolodex')]/a/span"));
		String profileNameInCaps = profileName.toUpperCase();
		char ch = profileNameInCaps.charAt(0);
		WebElement alphabetToSelect = driver
				.findElement(By.xpath(".//*[contains(@id,'_rolodex')]/a/span[text()='" + ch + "']"));
		return Link.click("Alphabet To Select", alphabetToSelect);
	}

	public boolean selectProfile(String profileName) {
		Sync.waitForObject(driver, "Profile to Select", By.xpath(".//*[@id='ext-gen11']/div"));
		Sync.waitForSeconds("WAIT_3");
		boolean status=false;
		List<WebElement> profileNames = driver
				.findElements(By.xpath(".//*[contains(@id,'_ProfileName')]/a/span"));
		Iterator<WebElement> itr = profileNames.iterator();
		while (itr.hasNext()) {
			WebElement profile= itr.next();
			if (profile.getText().equals(profileName)) {
				status=Link.click(profileName, profile);
				break;
			}
		}
		return status;
	}

	public boolean clickViewUsersButton() {
		Frames.switchToFrame(driver, "//iframe[contains(@id,'vfFrameId_')]");
		Robot r = null;
		try {
			r = new Robot();
		} catch (AWTException e) {
			System.out.println("AWTException");
			e.printStackTrace();
		} // construct a Robot object for default screen
		r.mouseMove(100, 200);// move mouse to java coords 1360, 7
		Sync.waitForObject(driver, "ViewUserBtn", viewUsersBtn);
		return Button.click("View Users", viewUsersBtn);

	}
}
