package com.common.toolbox;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.common.util.ResultUtil;

public class Button {

	public static boolean click(String strLogicalName, WebElement element) {
		boolean isButtonClicked = false;
		try {
			element.click();
			isButtonClicked = true;
			ResultUtil.report("PASS", "Clicked " + strLogicalName + " button");
		} catch (Exception e) {
			ResultUtil.report("FAIL",
					"Unable to click " + strLogicalName + " button. Exception occurred:" + e.getMessage());
		}

		return isButtonClicked;
	}

	public static boolean jsClick(String strLogicalName, WebDriver driver, WebElement element) {
		boolean isButtonClicked = false;
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].click();", element);
			isButtonClicked = true;
			ResultUtil.report("PASS", "Clicked " + strLogicalName + " button");
		} catch (Exception e) {
			ResultUtil.report("FAIL",
					"Unable to click " + strLogicalName + " button Exception occurred:" + e.getMessage());
		}

		return isButtonClicked;
	}

	public static boolean verifyObject(String strLogicalName,WebElement element) {
		boolean isVerified = false;
		try {
			if (element.isDisplayed()) {
				isVerified = true;
				ResultUtil.report("PASS", "Verified Object " + strLogicalName );
			}
		} catch (Exception e) {
			ResultUtil.report("FAIL",
					"Unable to Verify " + strLogicalName + " Exception occurred:" + e.getMessage());
		}
		return isVerified;
	}
	
}
