package com.common.toolbox;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.common.misc.Sync;
import com.common.util.ResultUtil;

public class Checkbox {

	public static boolean check(WebDriver driver, String strLogicalName, WebElement element) {
		boolean isValueChecked = false;
		try {
			Sync.waitForObject(driver, strLogicalName, element);
			if (!element.isSelected()) {
				element.click();
			}
			isValueChecked = true;
			ResultUtil.report("PASS", "Checked " + strLogicalName + " checkbox");
		} catch (Exception e) {
			ResultUtil.report("FAIL",
					"Unable to check " + strLogicalName + " checkbox, Exception occurred:" + e.getMessage());
		}

		return isValueChecked;
	}

	public static boolean verifyChecked(String strLogicalName, WebElement element) {
		boolean isValueChecked = false;
		if (element.getAttribute("title").equals("Checked")) {
			isValueChecked = true;
			ResultUtil.report("PASS", "Verified " + strLogicalName + " checkbox is checked");
		} else {
			isValueChecked = false;
			ResultUtil.report("FAIL", "" + strLogicalName + " checkbox is unchecked");
		}

		return isValueChecked;
	}

	public static boolean uncheck(WebDriver driver, String strLogicalName, WebElement element) {
		boolean isValueUnchecked = false;
		try {
			Sync.waitForObject(driver, strLogicalName, element);
			if (element.isSelected()) {
				element.click();
			}
			isValueUnchecked = true;
			ResultUtil.report("PASS", "Unchecked " + strLogicalName + " checkbox");
		} catch (Exception e) {
			ResultUtil.report("FAIL",
					"Unable to uncheck " + strLogicalName + " checkbox, Exception occurred:" + e.getMessage());
		}

		return isValueUnchecked;
	}

	public static boolean verifyUnchecked(String strLogicalName, WebElement element) {
		boolean isValueUnchecked = false;
		if (element.getAttribute("title").equals("Not Checked")) {
			isValueUnchecked = true;
			ResultUtil.report("PASS", "Verified " + strLogicalName + " checkbox is Unchecked");
		} else {
			isValueUnchecked = false;
			ResultUtil.report("FAIL", "" + strLogicalName + " checkbox is Checked");
		}

		return isValueUnchecked;
	}

	public static boolean verifyCheckbox(String strLogicalName, WebElement element, String StrValue) {
		boolean isValueUnchecked = false;
		String att = element.getAttribute("title");
		if (StrValue.equals("Yes")) {
			if (att.equals("Checked")) {
				isValueUnchecked = true;
				ResultUtil.report("PASS", "Verified " + strLogicalName + " checkbox is Checked");
			} else {
				isValueUnchecked = false;
				ResultUtil.report("FAIL", "" + strLogicalName + " checkbox is Unchecked");
			}
		} else {
			if (att.equals("Not Checked")) {
				isValueUnchecked = true;
				ResultUtil.report("PASS", "Verified " + strLogicalName + " checkbox is Unchecked");
			} else {
				isValueUnchecked = false;
				ResultUtil.report("FAIL", "" + strLogicalName + " checkbox is Checked");
			}
		}
		return isValueUnchecked;
	}
	 public static boolean selectCheckbox(String strLogicalName,WebDriver driver,WebElement element,String flags) {
     	boolean status = false;
 		if (flags.equals("Yes")) {
 			status = Checkbox.check(driver, strLogicalName, element);
 		} else if (flags.equals("No")) {
 			status = Checkbox.uncheck(driver, strLogicalName, element);
 		}
 		return status;
 	}
}
