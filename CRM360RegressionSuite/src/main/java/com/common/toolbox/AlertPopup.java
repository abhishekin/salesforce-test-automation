package com.common.toolbox;

import org.openqa.selenium.Alert;
import com.common.util.ResultUtil;

public class AlertPopup {

	public static boolean acceptAlert(String strLogicalName, Alert alt) {
		boolean isAlertAccepted = false;
		try {
			alt.accept();
			isAlertAccepted = true;
			ResultUtil.report("PASS", "Accepted " + strLogicalName + " popup");
		} catch (Exception e) {
			ResultUtil.report("FAIL",
					"Unable to accept " + strLogicalName + " popup. Exception occurred:" + e.getMessage());
		}

		return isAlertAccepted;
	}
	
	public static boolean dismissAlert(String strLogicalName, Alert alt) {
		boolean isAlertDismissed = false;
		try {
			alt.dismiss();;
			isAlertDismissed = true;
			ResultUtil.report("PASS", "Dismissed " + strLogicalName + " popup");
		} catch (Exception e) {
			ResultUtil.report("FAIL",
					"Unable to dismiss " + strLogicalName + " popup. Exception occurred:" + e.getMessage());
		}

		return isAlertDismissed;
	}

}
