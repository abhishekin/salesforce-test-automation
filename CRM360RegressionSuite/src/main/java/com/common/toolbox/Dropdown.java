package com.common.toolbox;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.common.util.ResultUtil;

public class Dropdown {

	public static boolean selectByVisibleText(String strLogicalName, WebElement ddl, String visibleText) {
		boolean isSelected = false;
		try {
			Select sel = new Select(ddl);
			sel.selectByVisibleText(visibleText);
			isSelected = true;
			ResultUtil.report("PASS", "Selected " + visibleText + " from " + strLogicalName + " dropdown");
		} catch (Exception e) {
			ResultUtil.report("FAIL", "Unable to select " + visibleText + " from " + strLogicalName
					+ " dropdown. Exception occurred:" + e.getMessage());
		}

		return isSelected;
	}

	public static boolean selectByValue(String strLogicalName, WebElement ddl, String value) {
		boolean isSelected = false;
		try {
			Select sel = new Select(ddl);
			sel.selectByValue(value);
			isSelected = true;
			ResultUtil.report("PASS", "Selected " + value + " from " + strLogicalName + " dropdown");
		} catch (Exception e) {
			ResultUtil.report("FAIL", "Unable to select " + value + " from " + strLogicalName
					+ " dropdown. Exception occurred:" + e.getMessage());
		}

		return isSelected;
	}

	public static boolean selectByIndex(String strLogicalName, WebElement ddl, int index) {
		boolean isSelected = false;
		try {
			Select sel = new Select(ddl);
			sel.selectByIndex(index);
			isSelected = true;
			ResultUtil.report("PASS", "Selected " + index + " from " + strLogicalName + " dropdown");
		} catch (Exception e) {
			ResultUtil.report("FAIL", "Unable to select " + index + " from " + strLogicalName
					+ " dropdown. Exception occurred:" + e.getMessage());
		}

		return isSelected;
	}

}
