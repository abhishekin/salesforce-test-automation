package com.common.toolbox;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.common.util.ResultUtil;

public class Textarea {
	
	public static boolean enterValueTextArea(String strLogicalName ,WebDriver driver,String xpath, WebElement element,
			String strValue) {

		boolean isValueEntered = false;
		try {
			
			Frames.switchToFrame(driver ,xpath);
			element.sendKeys(Keys.chord(Keys.CONTROL, "a"));
			element.sendKeys(Keys.DELETE);
			element.sendKeys(strValue);
			if (element.getText().equals(strValue)) {
				isValueEntered = true;
				ResultUtil.report("PASS", "Entered '" + strValue + "' in " + strLogicalName + " textbox");
				Frames.switchtoDefaultFrame(driver);
			} else {
				isValueEntered = false;
				ResultUtil.report("FAIL", "Unable to enter '" + strValue + "' in " + strLogicalName + " textbox");
			}
		} catch (Exception e) {
			ResultUtil.report("FAIL", "Unable to enter " + strValue + " value in " + strLogicalName
					+ " textbox. Exception occurred:" + e.getMessage());
		}

		return isValueEntered;

	}

}
