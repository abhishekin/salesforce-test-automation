package com.common.toolbox;

import org.openqa.selenium.WebElement;

import com.common.util.ResultUtil;

public class Image{
	
	public static boolean clickImage(String strLogicalName, WebElement element) {
		boolean isImageClicked = false;
		try {
			element.click();
			isImageClicked = true;
			ResultUtil.report("PASS", "Clicked " + strLogicalName + " link");
		} catch (Exception e) {
			ResultUtil.report("FAIL",
					"Unable to click " + strLogicalName + " image. Exception occurred:" + e.getMessage());
		}

		return isImageClicked;
	}

}
