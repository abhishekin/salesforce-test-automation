package com.common.toolbox;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.WebElement;

import com.common.util.ResultUtil;

public class Link {

	public static boolean click(String strLogicalName, WebElement element) {
		boolean isLinkClicked = false;
		try {
			element.click();
			isLinkClicked = true;
			ResultUtil.report("PASS", "Clicked " + strLogicalName + " link");
		} catch (Exception e) {
			ResultUtil.report("FAIL",
					"Unable to click " + strLogicalName + " link. Exception occurred:" + e.getMessage());
		}

		return isLinkClicked;
	}

	public static boolean verifyText(String strLogicalName, WebElement element, String strValue) {
		boolean isTextValid = false;

		try {
			if (element.getText().equals(strValue)) {
				isTextValid = true;
				ResultUtil.report("PASS", "Verified " + strLogicalName + " text");
			}
		} catch (Exception e) {
			isTextValid = false;
			ResultUtil.report("FAIL", "" + strLogicalName + "text verification failed. Expected Value: " + strValue
					+ "Actual Value: " + element.getText() + "Exception occurred: " + e.getMessage());
		}

		return isTextValid;
	}

	public static boolean verifyDate(String strLogicalName, WebElement element, String strValue) throws ParseException {
		boolean isDateValid = false;

		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		Date strValuetoDtExcel = df.parse(strValue);
		String strValuefromWebPage = element.getText();
		Date strValuetoDtWebPage = df.parse(strValuefromWebPage);

		try {
			if (strValuetoDtWebPage.equals(strValuetoDtExcel)) {
				isDateValid = true;
				ResultUtil.report("PASS", "Verified " + strLogicalName + " Date");
			}
		} catch (Exception e) {
			isDateValid = false;
			ResultUtil.report("FAIL", "" + strLogicalName + "text verification failed. Expected Value: " + strValue
					+ "Actual Value: " + element.getText() + "Exception occurred: " + e.getMessage());
		}

		return isDateValid;
	}
	
	public static boolean verifyDateLocal(String strLogicalName, WebElement element) throws ParseException {
		boolean isTextValid = false;
		DateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy");
		Date date = new Date();
		String dateInput =dateFormat.format(date); 
		System.out.println("       Today's Date " + dateInput);
		
		String detailPageDt =element.getText();
		Date date1=new SimpleDateFormat("dd/MM/yyyy").parse(detailPageDt);  
	    SimpleDateFormat newFormat = new SimpleDateFormat("dd/MMM/yyyy");
	    String finaldetailPageDt = newFormat.format(date1);
		System.out.println("       Salesforce Date " + finaldetailPageDt);

		try {
			if (finaldetailPageDt.equals(dateInput)) {
				isTextValid = true;
				ResultUtil.report("PASS", "Verified " + strLogicalName + " text");
			}
		} catch (Exception e) {
			isTextValid = false;
			ResultUtil.report("FAIL", "" + strLogicalName + "text verification failed. Expected Value: " + date
					+ "Actual Value: " + element.getText() + "Exception occurred: " + e.getMessage());
		}

		return isTextValid;
	}
	
	public static boolean verifyDateLocalForStageOppty(String strLogicalName, WebElement element) throws ParseException {
		boolean isTextValid = false;
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		String dateInput =dateFormat.format(date); 
		System.out.println("       Today's Date " + dateInput);
		String detailPageDt =element.getText();
		System.out.println("       Salesforce Date " + detailPageDt);

		try {
			if (detailPageDt.equals(dateInput)) {
				isTextValid = true;
				ResultUtil.report("PASS", "Verified " + strLogicalName + " text");
			}
		} catch (Exception e) {
			isTextValid = false;
			ResultUtil.report("FAIL", "" + strLogicalName + "text verification failed. Expected Value: " + date
					+ "Actual Value: " + element.getText() + "Exception occurred: " + e.getMessage());
		}

		return isTextValid;
	}

	
	

}
