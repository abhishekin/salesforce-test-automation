package com.common.toolbox;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jsoup.Jsoup;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.common.misc.Sync;
import com.common.util.ResultUtil;

public class Textbox {

	public static boolean enterValue(String strLogicalName, WebElement element, String strValue) {
		boolean isValueEntered = false;
		try {
			element.clear();
			element.sendKeys(strValue);
			if (element.getAttribute("value").equals(strValue)) {
				isValueEntered = true;
				ResultUtil.report("PASS", "Entered '" + strValue + "' in " + strLogicalName + " textbox");
			} else {
				isValueEntered = false;
				ResultUtil.report("FAIL", "Unable to enter '" + strValue + "' in " + strLogicalName + " textbox");
			}
		} catch (Exception e) {
			ResultUtil.report("FAIL", "Unable to enter " + strValue + " value in " + strLogicalName
					+ " textbox. Exception occurred:" + e.getMessage());
		}

		return isValueEntered;
	}
	
	public static boolean enterAutocompleteValue(String strLogicalName, WebElement element, String strValue, WebDriver driver) {
		boolean isValueEntered = false;
		try {
			element.clear();
			element.sendKeys(strValue);
			Sync.waitForSeconds("WAIT_2");
			element.sendKeys(Keys.ENTER);
			WebElement autoCompValue = driver.findElement(By.xpath("//table/tbody/tr[1]/td[1]/div/div[2]/div/a"));
			isValueEntered=	Link.click("Autocomplete Value", autoCompValue);
			if (isValueEntered = true) {
				ResultUtil.report("PASS", "Entered '" + strValue + "' in " + strLogicalName + " textbox");
			} else {
				isValueEntered = false;
				ResultUtil.report("FAIL", "Unable to enter '" + strValue + "' in " + strLogicalName + " textbox");
			}
		} catch (Exception e) {
			ResultUtil.report("FAIL", "Unable to enter " + strValue + " value in " + strLogicalName
					+ " textbox. Exception occurred:" + e.getMessage());
		}

		return isValueEntered;
	}
	
	public static boolean enterDateLocal(String strLogicalName, WebElement element ) {
		boolean isValueEntered = false;
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		String stringDate = dateFormat.format(date);
		System.out.println("       Today's Date " + stringDate);
		try {
			element.clear();
			element.sendKeys(stringDate);
			if (element.getAttribute("value").equals(stringDate)) {
				isValueEntered = true;
				ResultUtil.report("PASS", "Entered '" + stringDate + "' in " + strLogicalName + " textbox");
			} else {
				isValueEntered = false;
				ResultUtil.report("FAIL", "Unable to enter '" + stringDate + "' in " + strLogicalName + " textbox");
			}
		} catch (Exception e) {
			ResultUtil.report("FAIL", "Unable to enter " + stringDate + " value in " + strLogicalName
					+ " textbox. Exception occurred:" + e.getMessage());
		}

		return isValueEntered;
	}
	
	public static boolean enterPassword(String strLogicalName, WebElement element, String strValue) {
		boolean isValueEntered = false;
		try {
			element.clear();
			element.sendKeys(strValue);
			if (element.getAttribute("value").equals(strValue)) {
				isValueEntered = true;
				ResultUtil.report("PASS", "Entered " + "'Encrypted Password'" + " in " + strLogicalName );
			} else {
				isValueEntered = false;
				ResultUtil.report("FAIL", "Unable to enter " + "'Encrypted Password'" + " in " + strLogicalName);
			}
		} catch (Exception e) {
			ResultUtil.report("FAIL", "Unable to enter " + "'Encrypted Password'" + " value in " + strLogicalName
					+ " Exception occurred:" + e.getMessage());
		}

		return isValueEntered;
	}

	public static boolean jsEnterValue(String strLogicalName, WebDriver driver, WebElement element, String strValue) {
		boolean isValueEntered = false;
		String tagName = "iframe" ;
		Frames.switchToFrame(driver ,tagName);
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].value=arguments[1];", element, strValue);
			isValueEntered = true;
			ResultUtil.report("PASS", "Entered " + strValue + " in " + strLogicalName + " textbox");
		} catch (Exception e) {
			ResultUtil.report("FAIL", "Unable to enter " + strValue + " value in " + strLogicalName
					+ " textbox Exception occurred:" + e.getMessage());
		}

		return isValueEntered;
	}

	public static boolean verifyText(String strLogicalName, WebElement element, String strValue) {
		boolean isTextValid = false;
		String str = Jsoup.parse(element.getText()).text();
		if (str.equals(strValue)) {
			isTextValid = true;
			ResultUtil.report("PASS", "Verified " + strLogicalName + " text");
		} else {
			isTextValid = false;
			ResultUtil.report("FAIL", "" + strLogicalName + " text verification failed. Expected Value: " + strValue
					+ "Actual Value: " + str);
		}

		return isTextValid;
	}
	
	public static boolean verifyValueAttribute(String strLogicalName, WebElement element, String strValue) {
		boolean isTextValid = false;
		String str = element.getAttribute("value");
				if (str.equals(strValue)) {
			isTextValid = true;
			ResultUtil.report("PASS", "Verified " + strLogicalName + " text");
		} else {
			isTextValid = false;
			ResultUtil.report("FAIL", "" + strLogicalName + " text verification failed. Expected Value: " + strValue
					+ "Actual Value: " + str);
		}

		return isTextValid;
	}
	
	

	public static boolean verifyCurrencyAmount(String strLogicalName, WebElement element, String strValue) {
		boolean isTextValid = false;
		
		String convertedString1 = new DecimalFormat("#,###.00").format(Double.parseDouble(strValue));
		System.out.println("input"+convertedString1);
		String convertedString = Jsoup.parse(element.getText()).text();
		System.out.println("element"+convertedString);
		if (convertedString.equals(convertedString1)) {
			isTextValid = true;
			ResultUtil.report("PASS", "Verified " + strLogicalName + " text");
		} else {
			isTextValid = false;
			ResultUtil.report("FAIL", "" + strLogicalName + " text verification failed. Expected Value: " + convertedString1
					+ "Actual Value: " + convertedString);
		}

		return isTextValid;
	}

}
