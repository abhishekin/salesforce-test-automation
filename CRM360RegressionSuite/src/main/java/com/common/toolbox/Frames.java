package com.common.toolbox;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.common.util.ResultUtil;

public class Frames {
	
	WebDriver driver;
	
	public static void switchtoDefaultFrame(WebDriver driver) {
		try {
			driver.switchTo().defaultContent();
			ResultUtil.report("PASS", "Switched to " + "Default"+ " Frame");
		} catch (Exception e) {
			ResultUtil.report("FAIL",
					"Unable to Switch " + "Default" + " Frame. Exception occurred:" + e.getMessage());
		}
	}
	
	public static void switchToFrame(WebDriver driver,String xpath) {
		try {
			
			driver.switchTo().frame(driver.findElement(By.xpath(xpath)));
			ResultUtil.report("PASS", "Navigated to" + xpath + " Frame");
		} catch (Exception e) {
			ResultUtil.report("FAIL",
					"Unable to Switch " + xpath + " Frame. Exception occurred:" + e.getMessage());
		}
	}
}


