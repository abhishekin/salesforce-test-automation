package com.common.toolbox;

import java.util.ArrayList;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.common.util.ResultUtil;

public class Menu {
	
	public static boolean click(WebDriver driver, String strLogicalName, WebElement element) {
		boolean isMenuClicked = false;
		try {
			Actions act=new Actions(driver);
			act.moveToElement(element).click().build().perform();
			isMenuClicked = true;
			ResultUtil.report("PASS", "Clicked " + strLogicalName + " menu");
		} catch (Exception e) {
			ResultUtil.report("FAIL",
					"Unable to click " + strLogicalName + " menu. Exception occurred:" + e.getMessage());
		}

		return isMenuClicked;
	}
	
	
	public static boolean mouseOver(WebDriver driver, String strLogicalName, WebElement element) {
		boolean isMenuMouseOver = false;
		try {
			Actions actions = new Actions(driver);
			actions.moveToElement(element).perform();
			isMenuMouseOver = true;
			ResultUtil.report("PASS", "Hovered over " + strLogicalName + " menu");
		} catch (Exception e) {
			ResultUtil.report("FAIL",
					"Unable to hover over " + strLogicalName + " menu. Exception occurred:" + e.getMessage());
		}
		return isMenuMouseOver;

	}
	
	public static boolean switchToNewTab(WebDriver driver, String strLogicalName ) {
		boolean switchedToNewTab = false;
		try {
			ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
			//tabs.remove(OldTab);
		    driver.switchTo().window(tabs.get(1));
			ResultUtil.report("PASS", "Switched " + strLogicalName + " menu");
		} catch (Exception e) {
			ResultUtil.report("FAIL",
					"Unable to switch" + strLogicalName + " menu. Exception occurred:" + e.getMessage());
		}

		return switchedToNewTab;
	}
	
	
}
