package com.common.toolbox;

import org.jsoup.Jsoup;
import org.openqa.selenium.WebElement;

import com.common.util.ResultUtil;

public class ErrorMessage {
	
	public static boolean verifyErrorMessage(String strLogicalName, WebElement element, String strValue) {
		boolean isErrorMsgValid = false;
		String str = Jsoup.parse(element.getText()).text();
		//System.out.println(str);
		if (str.equals(strValue)) {
			isErrorMsgValid = true;
			ResultUtil.report("PASS", "Verified " + strLogicalName + " text");
		} else {
			isErrorMsgValid = false;
			ResultUtil.report("FAIL", "" + strLogicalName + " text verification failed. Expected Value: " + strValue
					+ "Actual Value: " + element.getText());
		}

		return isErrorMsgValid;
	}
	
	public static boolean verifyErrorMessageOpportunity(String strLogicalName, WebElement element, String strValue) {
		boolean isErrorMsgValid = false;
		//System.out.println(element);
		String str = Jsoup.parse(element.getAttribute("title")).text();
		//System.out.println(str);
		if (str.equalsIgnoreCase(strValue)) {
			isErrorMsgValid = true;
			ResultUtil.report("PASS", "Verified " + strLogicalName + " text");
		} else {
			isErrorMsgValid = false;
			ResultUtil.report("FAIL", "" + strLogicalName + " text verification failed. Expected Value: " + strValue
					+ "Actual Value: " + element.getAttribute("title"));
		}

		return isErrorMsgValid;
	}

}
