package com.common.util;

import org.testng.SkipException;

public class SuiteUtil {

	public static void checkTestCaseToRun(String flag) {
		switch (flag) {
		case "Y":
			break;
		case "N":
			throw new SkipException("CaseToRun Flag Is 'N' Or Blank. So Skipping Execution ");
		default:
			throw new IllegalArgumentException(
					"Invalid Option specified in 'Execute (Y/N)' column. Please specify Y or N");
			
		}
	}
}
