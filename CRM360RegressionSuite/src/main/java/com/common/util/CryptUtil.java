package com.common.util;

import javax.xml.bind.DatatypeConverter;

public class CryptUtil {

	/********************************************************************
	 * 
	 * decryptXOR(String message, String key)
	 * 
	 * Uses provided key to decrypt provided message encrypted using the same
	 * key
	 * 
	 *********************************************************************/

	public static String decryptXOR(String message, String key) {
		try {
			if (message == null || key == null)
				return null;
			
			char[] keys = key.toCharArray();
			byte[] mesg = DatatypeConverter.parseBase64Binary(message);

			int ml = mesg.length;
			int kl = keys.length;
			char[] newmsg = new char[ml];

			for (int i = 0; i < ml; i++) {
				newmsg[i] = (char) (mesg[i] ^ keys[i % kl]);
			}
			mesg = null;
			keys = null;
			return new String(newmsg);
		} catch (Exception e) {
			return null;
		}
	}

}
