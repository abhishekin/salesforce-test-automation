package com.common.util;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.common.misc.SharedDriver;

public class BrowserUtil {

	public static final String USERNAME = TestDataUtil.testData.getCellData("LoginAndConfig", "BrowserStackUsername",
			EnvironmentUtil.environmentRowNum);
	public static final String AUTOMATE_KEY = TestDataUtil.testData.getCellData("LoginAndConfig",
			"BrowserStackPassword", EnvironmentUtil.environmentRowNum);
	public static final String URL = "http://" + USERNAME + ":" + AUTOMATE_KEY + "@hub.browserstack.com/wd/hub";

	public static WebDriver getDriver(String strBrowser, String strURL) {
		WebDriver driver = null;

		try {
			if (TestDataUtil.testData.getCellData("LoginAndConfig", "LocalOrRemote", EnvironmentUtil.environmentRowNum)
					.equalsIgnoreCase("local")) {
				if (strBrowser.equalsIgnoreCase("IE")) {
					System.setProperty("webdriver.ie.driver", "drivers/IEDriverServer.exe");
					DesiredCapabilities ieCap = new DesiredCapabilities();
					ieCap.setCapability("ignoreZoomSetting", true);
					ieCap.setCapability("requireWindowFocus", true);
					ieCap.setCapability("enablePersistentHover", false);
					driver = new InternetExplorerDriver(ieCap);
				}

				else if (strBrowser.equalsIgnoreCase("FIREFOX")) {
					driver = new FirefoxDriver();

				} else if (strBrowser.equalsIgnoreCase("CHROME")) {
					System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");
					driver = new ChromeDriver();
				}
			} else if (TestDataUtil.testData
					.getCellData("LoginAndConfig", "LocalOrRemote", EnvironmentUtil.environmentRowNum)
					.equalsIgnoreCase("browserstack"))

			{
				if (TestDataUtil.testData
						.getCellData("LoginAndConfig", "ProxyRequired", EnvironmentUtil.environmentRowNum)
						.equalsIgnoreCase("Yes")) {
					System.out.println("Proxy Selected Yes");
					System.getProperties().put("http.proxyHost", "proxy-ap.shell.com");
					System.getProperties().put("http.proxyPort", "8080");
					System.getProperties().put("https.proxyHost", "proxy-ap.shell.com");
					System.getProperties().put("https.proxyPort", "8080");
				}

				DesiredCapabilities capability = new DesiredCapabilities();
				capability.setCapability("browserName", strBrowser);
				capability.setCapability("browserVersion", TestDataUtil.testData.getCellData("LoginAndConfig",
						"browserVersion", EnvironmentUtil.environmentRowNum));
				capability.setCapability("os",
						TestDataUtil.testData.getCellData("LoginAndConfig", "os", EnvironmentUtil.environmentRowNum));
				capability.setCapability("os_version", TestDataUtil.testData.getCellData("LoginAndConfig", "os_version",
						EnvironmentUtil.environmentRowNum));
				capability.setCapability("project", TestDataUtil.testData.getCellData("LoginAndConfig", "project",
						EnvironmentUtil.environmentRowNum));
				capability.setCapability("browserstack.debug", TestDataUtil.testData.getCellData("LoginAndConfig",
						"browserstack.debug", EnvironmentUtil.environmentRowNum));

				capability.setCapability("browserstack.timezone", TestDataUtil.testData.getCellData("LoginAndConfig",
						"BrowserStackTimezone", EnvironmentUtil.environmentRowNum));
				capability.setCapability("browserstack.selenium_version", TestDataUtil.testData.getCellData("LoginAndConfig",
						"SeleniumVersion", EnvironmentUtil.environmentRowNum));

				driver = null;
				try {
					driver = new RemoteWebDriver(new java.net.URL(URL), capability);
				} catch (MalformedURLException e) {
					System.out.println("Incorrect URL");
					e.printStackTrace();
				}
			}
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.get(strURL);
			ResultUtil.report("PASS", "Opened " + strURL + " on " + strBrowser);

		} catch (Exception e) {
			ResultUtil.report("FAIL",
					"Unable to open " + strURL + " on " + strBrowser + " Exception occurred:" + e.getMessage());
		}

		return driver;
	}

	public static void closeBrowser() {
		ResultUtil.test = ResultUtil.reporter.startTest("Close Browsers");
		try {
			SharedDriver.driver.quit();
			ResultUtil.report("INFO", "Closed browser(s)");
		} catch (Exception e) {
			ResultUtil.report("INFO", "Unable to close browser(s)-Exception occurred:" + e.getMessage());
		}
		ResultUtil.reporter.endTest(ResultUtil.test);
		ResultUtil.reporter.flush();
	}

}
