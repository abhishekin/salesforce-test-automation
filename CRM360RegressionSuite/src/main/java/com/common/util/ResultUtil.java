package com.common.util;

import java.io.File;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.common.misc.SharedDriver;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class ResultUtil {

	public static final Logger log;
	public static final ExtentReports reporter;
	public static ExtentTest test;

	static {
		log = LogManager.getLogger(ResultUtil.class.getName());
		reporter = new ExtentReports("results/Dashboard.html", true);
		reporter.loadConfig(new File("config/extent-config.xml"));
	}

	public static void report(String strStatus, String strDesc) {
		if (strStatus.equalsIgnoreCase("PASS")) {
			test.log(LogStatus.PASS, strDesc);
			log.info("PASS - " + strDesc);
			System.out.println("PASS - " + strDesc);
		} else if (strStatus.equalsIgnoreCase("FAIL")) {
			test.log(LogStatus.FAIL, strDesc);
			test.log(LogStatus.INFO, "Snapshot below: " + test.addScreenCapture(takeScreenshot()));
			log.error("FAIL - " + strDesc);
			System.out.println("FAIL - " + strDesc);
		} else if (strStatus.equalsIgnoreCase("INFO")) {
			test.log(LogStatus.INFO, strDesc);
		    log.info("INFO - " + strDesc);
			System.out.println("INFO - " + strDesc);
		} else if (strStatus.equalsIgnoreCase("SKIP")) {
			test.log(LogStatus.SKIP, strDesc);
			log.info("SKIP - " + strDesc);
			System.out.println("SKIP - " + strDesc);
		}

		// Hard Stop
		if (strStatus.equalsIgnoreCase("FAIL")
				&& HelperUtil.executionConfigMap.get("HARD_STOP").equalsIgnoreCase("Y")) {
			BrowserUtil.closeBrowser();
			System.exit(0);
		}
	}

	public static String takeScreenshot() {
		String strFileName = UUID.randomUUID().toString() + ".png";
		try {
			EventFiringWebDriver event = new EventFiringWebDriver(SharedDriver.driver);
			File srcFile = event.getScreenshotAs(OutputType.FILE);
			File destFile = new File("results/screenshots/" + strFileName);
			FileUtils.copyFile(srcFile, destFile);
		} catch (Exception e) {

		}
		return "./screenshots/" + strFileName;
	}
}
