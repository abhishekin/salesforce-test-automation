package com.common.util;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CleanupUtil {

	public static void cleanupFolder(String folder) {
		File directory = new File(folder);

		if(null != directory){
	
			File[] files = directory.listFiles();
			if(null != files && files.length != 0){
				for (File file : files) {
					if (file.isFile()) {
						file.delete();
						System.out.println("File '" + file.getName() + "' deleted from " + file.getPath());
					}
				}
			}
		}
	}

	public static void archiveResults() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy_EEE_HH-mm-ss a_z");
		String formattedDate = sdf.format(date);
		final String INPUT_FOLDER = "results";
		final String ZIPPED_FOLDER = "archive/" + "LastRunResults_ArchivedOn_" + formattedDate + ".zip";
		try {
			ZipUtil.zip(INPUT_FOLDER, ZIPPED_FOLDER);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
