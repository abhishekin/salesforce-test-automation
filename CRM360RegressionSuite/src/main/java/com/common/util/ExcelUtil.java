package com.common.util;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class ExcelUtil {

public static Iterator<Object[]> getTestData(String strWorkbookPath,String strWorksheetName){
	List<Object[]> data = new ArrayList<Object[]>();
	
	int inRowCounter=1;
	
	try{
		File file = new File(strWorkbookPath);
    
		//Get the workbook instance for XLS file
		Workbook workbook = WorkbookFactory.create(file);
		
		//Get first sheet from the workbook
		Sheet sheet = workbook.getSheet(strWorksheetName);
	
		//Get iterator to all the rows in current sheet
		Iterator<Row> rowIterator = sheet.rowIterator();
		
		Row firstRow=rowIterator.next();
	
		Map<String,String> columnNamesMap=getColumnNames(firstRow);
		
		while(rowIterator.hasNext()){
			Iterator<Cell> cellIterator=rowIterator.next().cellIterator();
			Map<String,String> rowMap=new LinkedHashMap<String, String>();
			for(Entry<String, String> entry:columnNamesMap.entrySet()){
				String strColumnName=entry.getKey().toString();
				String strValue="";
			    try{
			     Cell cell=cellIterator.next();
			     if(cell!=null){strValue=cell.toString();}
			    }catch(Exception e){}
				rowMap.put(strColumnName, strValue.trim());
			}
			
			if(rowMap.get("Execute").equalsIgnoreCase("Y")){	
				rowMap.put("Iteration", ""+inRowCounter);
				data.add(new Object[]{rowMap});
				inRowCounter++;
			}
		}
		
	workbook.close();
	}
	catch(Exception e){
		e.printStackTrace();
	}
	return data.iterator();
}

private static Map<String,String> getColumnNames(Row row){
	Map<String,String> columnNamesMap=new LinkedHashMap<String, String>();
	
	Iterator<Cell> cells=row.cellIterator();
	
	while(cells.hasNext()){
		String strColumnName=cells.next().toString();
		columnNamesMap.put(strColumnName, strColumnName);
	}
	
	return columnNamesMap;
}
}
