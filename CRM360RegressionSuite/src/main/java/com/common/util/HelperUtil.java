package com.common.util;

import java.util.Map;

import com.common.misc.*;

public class HelperUtil {

	public static Map<String, String> executionConfigMap;

	static {
		executionConfigMap = PropertyFileUtil.getPropertiesAsMap(Constants.EXECUTION_CONFIG_FILE);
	}

}