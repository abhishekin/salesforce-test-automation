package com.common.misc;

import org.openqa.selenium.WebDriver;

import com.pages.crm360.*;

public class PageContainer {

	public WebDriver driver;
	public LoginPage loginPage;
	public GlobalHeader globalHeader;
	public SetupPage setupPage;
	public ProfilesSelectionPage profilesSelectionPage;
	public UserSelectionPage userSelectionPage;
	public AllTabsPage allTabsPage;
	public GlobalSearchResultPage globalSearchResultPage;
	public RecordTypeSelectionPage recordTypeSelectionPage;
	public CommonButtonPage commonButtonPage;
	public AccountShipToCreatePage accountShipToCreatePage;
	public AccountSoldToCreatePage accountSoldToCreatePage;
	public AccountShipToDetailPage accountShipToDetailPage;
	public AccountSoldToDetailPage accountSoldToDetailPage;
	public OpportunityCommonErrorPage opportunityCommonErrorPage;
	public LeadCreatePage leadCreatePage;//--Added by Ashmita(8/31)
	public PageContainer(WebDriver driver) {
		this.driver = driver;
		initPages();
	}

	private void initPages() {
		loginPage = new LoginPage(driver);
		globalHeader = new GlobalHeader(driver);
		setupPage = new SetupPage(driver);
		profilesSelectionPage = new ProfilesSelectionPage(driver);
		userSelectionPage = new UserSelectionPage(driver);
		allTabsPage = new AllTabsPage(driver);
		globalSearchResultPage = new GlobalSearchResultPage(driver);
		commonButtonPage = new CommonButtonPage(driver);
		recordTypeSelectionPage = new RecordTypeSelectionPage(driver);
	    accountShipToCreatePage= new AccountShipToCreatePage(driver);
	    accountSoldToCreatePage= new AccountSoldToCreatePage(driver);
	    accountShipToDetailPage = new AccountShipToDetailPage(driver);
	    accountSoldToDetailPage = new AccountSoldToDetailPage(driver);
		opportunityCommonErrorPage = new OpportunityCommonErrorPage(driver);
		leadCreatePage = new LeadCreatePage(driver);//--Added by Ashmita(8/31)
	}
	
}
