package com.common.misc;

import org.openqa.selenium.WebDriver;
import com.common.util.*;

/**
 * The Class SharedDriver.
 */
public class SharedDriver {

	/** The driver. */
	public static WebDriver driver;

	/** The page container. */
	public static PageContainer pageContainer;

	/**
	 * Creates the driver.
	 */
	public static void createDriver() {
		driver = BrowserUtil.getDriver(
				TestDataUtil.testData.getCellData("LoginAndConfig", "browserName", EnvironmentUtil.environmentRowNum),
				TestDataUtil.testData.getCellData("LoginAndConfig", "URL", EnvironmentUtil.environmentRowNum));
		pageContainer = new PageContainer(driver);
	}
}