package com.common.misc;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.common.util.*;

public class Sync {

	public static void waitForPageLoad(WebDriver driver) {
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}

	public static boolean waitForObject(WebDriver driver, String strLogicalName, WebElement element) {
		boolean blResult = false;
		try {
			new WebDriverWait(driver, getWaitTime(Constants.OBJECT_WAIT_TIME))
					.until(ExpectedConditions.visibilityOf(element));
		} catch (Exception e) {
		}
		return blResult;

	}

	public static WebElement waitForObject(WebDriver driver, String strLogicalName, By by) {
		WebElement element = null;
		try {
			element = new WebDriverWait(driver, getWaitTime(Constants.OBJECT_WAIT_TIME))
					.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(by)).get(0);
		} catch (Exception e) {
		}
		return element;

	}

	public static boolean waitForFindElements(WebDriver driver, String strLogicalName, By by) {
		boolean blResult = false;
		try {
			 new WebDriverWait(driver, getWaitTime(Constants.OBJECT_WAIT_TIME))
					.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(by));
		} catch (Exception e) {
		}
		return blResult;
	}
	
	private static long getWaitTime(String strWaitType) {
		long lngWaitTime = 0;

		try {
			String strWaitTime = HelperUtil.executionConfigMap.get(strWaitType);
			String strWaitNumber = HelperUtil.executionConfigMap.get(Constants.WAIT_NUMBER);
			lngWaitTime = Long.parseLong(strWaitTime) * Long.parseLong(strWaitNumber);
		} catch (Exception e) {

		}

		return lngWaitTime;
	}

	public static void waitForSeconds(String strWaitName) {
		try {
			Thread.sleep(getWaitTime(strWaitName) * 1000);
		} catch (Exception e) {

		}
	}

	public static boolean waitUntilObjectDisappears(WebDriver driver, String strLogicalName, By by) {
		boolean blResult = false;
		try {
			new WebDriverWait(driver, getWaitTime(Constants.OBJECT_WAIT_TIME))
					.until(ExpectedConditions.invisibilityOfElementLocated(by));
		} catch (Exception e) {
		}
		return blResult;

	}
	  public static boolean waitForFrame(WebDriver driver, String strLogicalName, String frameName) {
			boolean blResult = false;
			try {
				new WebDriverWait(driver, getWaitTime(Constants.OBJECT_WAIT_TIME))
						.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frameName));
			} catch (Exception e) {
			}
			return blResult;

		}
	  public static boolean waitUntilFrameAppearsXpath(WebDriver driver, String strLogicalName, By by) {
			boolean blResult = false;
			try {
				new WebDriverWait(driver, getWaitTime(Constants.OBJECT_WAIT_TIME))
						.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(by));
			} catch (Exception e) {
			}
			return blResult;

		}
}
