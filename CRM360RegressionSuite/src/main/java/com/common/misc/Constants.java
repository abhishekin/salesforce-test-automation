package com.common.misc;

public class Constants {

	public static String EXECUTION_CONFIG_FILE = "config/executionConfig.properties";
	public static String WAIT_NUMBER = "WAIT_NUMBER";
	public static String OBJECT_WAIT_TIME = "OBJECT_WAIT_TIME";
	public static String PAGE_LOAD_TIME = "PAGE_LOAD_TIME";
	public static String SCRIPT_WAIT_TIME = "SCRIPT_WAIT_TIME";
	public static String WAIT_1 = "WAIT_1";
	public static String WAIT_2 = "WAIT_2";
	public static String WAIT_3 = "WAIT_3";
	public static String WAIT_5 = "WAIT_5";
	public static String WAIT_6 = "WAIT_6";
	public static String WAIT_7 = "WAIT_7";
	public static String WAIT_8 = "WAIT_8";
	public static String WAIT_9 = "WAIT_9";
	public static String WAIT_10 = "WAIT_10";
	public static String WAIT_11 = "WAIT_11";
	
}
