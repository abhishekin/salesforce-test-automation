package com.scripts.crm360;

import com.common.misc.SharedDriver;
import com.common.util.TestDataUtil;

public class AccountVerifyScript {
	
	//Verifying ShipTo Account

	public static void verifyShipToAccount(String sheetName, int rowNum) {
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.allTabsPage.clickAccountTab());
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.commonButtonPage.clickListView());
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.commonButtonPage.clickMyNGSAPAccount());
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.globalSearchResultPage
				.openAccount(TestDataUtil.testData.getCellData(sheetName, "AccountName", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.commonButtonPage.clickDetailsTab());
		verifyShipToAccountInfo(sheetName, rowNum);
		verifyShipToAccountAddress(sheetName, rowNum);

	}

	private static void verifyShipToAccountInfo(String sheetName, int rowNum) {

		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountShipToDetailPage
				.verifyAccountName(TestDataUtil.testData.getCellData(sheetName, "AccountName", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountShipToDetailPage
				.verifyPhone(TestDataUtil.testData.getCellData(sheetName, "Phone", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountShipToDetailPage
				.verifyAccountRecordType(TestDataUtil.testData.getCellData(sheetName, "AccountType", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountShipToDetailPage
				.verifyFax(TestDataUtil.testData.getCellData(sheetName, "Fax", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountShipToDetailPage
				.verifyRoles(TestDataUtil.testData.getCellData(sheetName, "Roles", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountShipToDetailPage
				.verifyWebsite(TestDataUtil.testData.getCellData(sheetName, "Website", rowNum)));
		/*SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountShipToDetailPage
				.verifyParentAccount(TestDataUtil.testData.getCellData(sheetName, "ParentAccount", rowNum)));*/
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountShipToDetailPage
				.verifyAEResponsible(TestDataUtil.testData.getCellData(sheetName, "AEResponsible", rowNum)));

		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountShipToDetailPage
				.verifyProductLine(TestDataUtil.testData.getCellData(sheetName, "ProductLine", rowNum)));

		/*SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountShipToDetailPage
				.verifyCRCResponsible(TestDataUtil.testData.getCellData(sheetName, "CRCResponsible", rowNum)));*/
	}

	private static void verifyShipToAccountAddress(String sheetName, int rowNum) {

		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountShipToDetailPage
				.verifyShippingStreet(TestDataUtil.testData.getCellData(sheetName, "ShippingStreet", rowNum)+","));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountShipToDetailPage
				.verifyShippingCity(TestDataUtil.testData.getCellData(sheetName, "ShippingCity", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountShipToDetailPage
				.verifyShippingState(TestDataUtil.testData.getCellData(sheetName, "ShippingState", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountShipToDetailPage
				.verifyShippingZip(TestDataUtil.testData.getCellData(sheetName, "ShippingZip", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountShipToDetailPage
				.verifyShippingCountry(TestDataUtil.testData.getCellData(sheetName, "ShippingCountry", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountShipToDetailPage
				.verifyDescription(TestDataUtil.testData.getCellData(sheetName, "Description", rowNum)));

	}

	
	
	//Verifying SoldTo Account
	
	
	public static void verifySoldToAccount(String sheetName, int rowNum) {
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.allTabsPage.clickAccountTab());
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.commonButtonPage.clickListView());
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.commonButtonPage.clickMyNGSAPAccount());
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.globalSearchResultPage
				.openAccount(TestDataUtil.testData.getCellData(sheetName, "AccountName", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.commonButtonPage.clickDetailsTab());
		verifySoldToAccountInfo(sheetName, rowNum);
		verifySoldToAccountAddress(sheetName, rowNum);
		verifySoldToAccountAttributes(sheetName, rowNum);

	}

	private static void verifySoldToAccountInfo(String sheetName, int rowNum) {

		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountSoldToDetailPage
				.verifyAccountName(TestDataUtil.testData.getCellData(sheetName, "AccountName", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountSoldToDetailPage
				.verifyPhone(TestDataUtil.testData.getCellData(sheetName, "Phone", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountSoldToDetailPage
				.verifyAccountRecordType(TestDataUtil.testData.getCellData(sheetName, "AccountRecordType", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountSoldToDetailPage
				.verifyFax(TestDataUtil.testData.getCellData(sheetName, "Fax", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountSoldToDetailPage
				.verifyRoles(TestDataUtil.testData.getCellData(sheetName, "Roles", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountSoldToDetailPage
				.verifyWebsite(TestDataUtil.testData.getCellData(sheetName, "WebSite", rowNum)));

		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountSoldToDetailPage
				.verifyAEResponsible(TestDataUtil.testData.getCellData(sheetName, "AEResponsible", rowNum)));

		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountSoldToDetailPage
				.verifyProductLine(TestDataUtil.testData.getCellData(sheetName, "ProductLine", rowNum)));

		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountSoldToDetailPage
				.verifyCRCResponsible(TestDataUtil.testData.getCellData(sheetName, "CRCResponsible", rowNum)));
	}

	private static void verifySoldToAccountAddress(String sheetName, int rowNum) {

		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountSoldToDetailPage
				.verifyStreet(TestDataUtil.testData.getCellData(sheetName, "Street", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountSoldToDetailPage
				.verifyCity(TestDataUtil.testData.getCellData(sheetName, "City", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountSoldToDetailPage
				.verifyState(TestDataUtil.testData.getCellData(sheetName, "State", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountSoldToDetailPage
				.verifyZip(TestDataUtil.testData.getCellData(sheetName, "Zip", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountSoldToDetailPage
				.verifyCountry(TestDataUtil.testData.getCellData(sheetName, "Country", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountSoldToDetailPage
				.verifyDescription(TestDataUtil.testData.getCellData(sheetName, "Description", rowNum)));

	}
	
	private static void verifySoldToAccountAttributes(String sheetName, int rowNum) {

		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountSoldToDetailPage
				.verifyAnnualRevenue(TestDataUtil.testData.getCellData(sheetName, "AnnualRevenue", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountSoldToDetailPage
				.verifySegment(TestDataUtil.testData.getCellData(sheetName, "Segment", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountSoldToDetailPage
				.verifyState(TestDataUtil.testData.getCellData(sheetName, "State", rowNum)));
		

	}
}
