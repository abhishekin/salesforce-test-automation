package com.scripts.crm360;

import com.common.misc.SharedDriver;
import com.common.util.TestDataUtil;

/**
 * Created by Ashmita Bhattacharyya on 31/Aug/2017.
 */

public class LeadCreateScript extends SoftAssertScript {

	public static void createLead(String sheetName, int rowNum) {
		// SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.globalHeader.clickAllTabsImage());
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.allTabsPage.clickLeadsTab());
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.commonButtonPage.clickNewLead());
	}

	public static void enterLeadInformation(String sheetName, int rowNum) {

		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.leadCreatePage
				.selectSalutation(TestDataUtil.testData.getCellData(sheetName, "Salutation", rowNum)));

		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.leadCreatePage
				.enterFirstName(TestDataUtil.testData.getCellData(sheetName, "FirstName", rowNum)));

		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.leadCreatePage
				.enterMiddleName(TestDataUtil.testData.getCellData(sheetName, "MiddleName", rowNum)));

		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.leadCreatePage
				.enterLastName(TestDataUtil.testData.getCellData(sheetName, "LastName", rowNum)));

		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.leadCreatePage
				.enterSuffix(TestDataUtil.testData.getCellData(sheetName, "Suffix", rowNum)));

		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.leadCreatePage
				.enterTitle(TestDataUtil.testData.getCellData(sheetName, "Title", rowNum)));

		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.leadCreatePage
				.enterCompany(TestDataUtil.testData.getCellData(sheetName, "Company", rowNum)));

		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.leadCreatePage
				.enterAnnualRevenue(TestDataUtil.testData.getCellData(sheetName, "AnnualRevenue", rowNum)));

		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.leadCreatePage
				.selectLeadSource(TestDataUtil.testData.getCellData(sheetName, "LeadSource", rowNum)));

		/*
		 * Need to include method to select from Multi Picklist for Product Line
		 */

		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.leadCreatePage
				.selectSalesRegion(TestDataUtil.testData.getCellData(sheetName, "SalesRegion", rowNum)));

		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.leadCreatePage
				.enterDescription(TestDataUtil.testData.getCellData(sheetName, "Description", rowNum)));

		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.leadCreatePage
				.selectRating(TestDataUtil.testData.getCellData(sheetName, "Rating", rowNum)));

		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.leadCreatePage.selectPreferredMethodOfContact(
				TestDataUtil.testData.getCellData(sheetName, "PreferredMethodOfContact", rowNum)));

		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.leadCreatePage
				.enterOfficePhone(TestDataUtil.testData.getCellData(sheetName, "OfficePhone", rowNum)));

		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.leadCreatePage
				.enterMobile(TestDataUtil.testData.getCellData(sheetName, "Mobile", rowNum)));

		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.leadCreatePage
				.enterFax(TestDataUtil.testData.getCellData(sheetName, "Fax", rowNum)));

		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.leadCreatePage
				.enterEmail(TestDataUtil.testData.getCellData(sheetName, "Email", rowNum)));

		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.leadCreatePage
				.enterWebsite(TestDataUtil.testData.getCellData(sheetName, "Website", rowNum)));

		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.leadCreatePage
				.enterCountry(TestDataUtil.testData.getCellData(sheetName, "Country", rowNum)));

		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.leadCreatePage
				.enterStreet(TestDataUtil.testData.getCellData(sheetName, "Street", rowNum)));

		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.leadCreatePage
				.enterCity(TestDataUtil.testData.getCellData(sheetName, "City", rowNum)));

		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.leadCreatePage
				.enterStateProvince(TestDataUtil.testData.getCellData(sheetName, "StateProvince", rowNum)));

		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.leadCreatePage
				.enterZip(TestDataUtil.testData.getCellData(sheetName, "ZIP", rowNum)));
	}

	public static void enterAddress(String sheetName, int rowNum) {
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.leadCreatePage
				.enterStreet(TestDataUtil.testData.getCellData(sheetName, "Street", rowNum)));

		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.leadCreatePage
				.enterCity(TestDataUtil.testData.getCellData(sheetName, "City", rowNum)));

		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.leadCreatePage
				.enterStateProvince(TestDataUtil.testData.getCellData(sheetName, "StateProvince", rowNum)));

		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.leadCreatePage
				.enterZip(TestDataUtil.testData.getCellData(sheetName, "Zip", rowNum)));

		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.leadCreatePage
				.enterCountry(TestDataUtil.testData.getCellData(sheetName, "Country", rowNum)));

	}

	public static void enterSystemInformation(String sheetName, int rowNum) {
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.leadCreatePage
				.selectLeadStatus(TestDataUtil.testData.getCellData(sheetName, "LeadStatus", rowNum)));
	}
	
	public static void saveNewLead() {
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.commonButtonPage.clickSaveNewLead());
	}

}
