package com.scripts.crm360;


import com.common.misc.SharedDriver;
import com.common.util.TestDataUtil;

public class LeadEditScript {

	public static void editLead(String sheetName, int rowNum) {
		String LeadName = TestDataUtil.testData.getCellData(sheetName, "salutation", rowNum) + " "
				+ TestDataUtil.testData.getCellData(sheetName, "FirstName", rowNum) + " "
				+ TestDataUtil.testData.getCellData(sheetName, "LastName", rowNum);
		System.out.println(LeadName);
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.globalSearchResultPage.openLead(LeadName));
		//SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.detailsPageCommonButtons.clickEditButton());
	}

	
}