package com.scripts.crm360;

import org.openqa.selenium.WebDriver;

import com.common.misc.SharedDriver;
import com.common.misc.Sync;
import com.common.util.ResultUtil;

public class LogoutScript {
	
  public static WebDriver driver;

	public static void performUserLogout() throws Exception {
		ResultUtil.test = ResultUtil.reporter.startTest("Logout User");
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.globalHeader.clickLogoutLink());
		Sync.waitForSeconds("WAIT_5");
	}
}
