package com.scripts.crm360;

import com.common.misc.SharedDriver;
import com.common.misc.Sync;
import com.common.util.TestDataUtil;

public class AccountCreateScript extends SoftAssertScript {

	
	
	//Create Ship To Account
	public static void createShipToAccount(String sheetName, int rowNum) {
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.allTabsPage.clickAccountTab());
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.commonButtonPage.clickNewAccount());
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.recordTypeSelectionPage.selectShipToRadio());
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.commonButtonPage.clickNext());
	}

	public static void enterAccountInformation(String sheetName, int rowNum) {
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountShipToCreatePage
				.enterAccountName(TestDataUtil.testData.getCellData(sheetName, "AccountName", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountShipToCreatePage
				.enterPhone(TestDataUtil.testData.getCellData(sheetName, "Phone", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountShipToCreatePage
				.enterFax(TestDataUtil.testData.getCellData(sheetName, "Fax", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountShipToCreatePage
				.enterRoles(TestDataUtil.testData.getCellData(sheetName, "Roles", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountShipToCreatePage
				.enterWebsite(TestDataUtil.testData.getCellData(sheetName, "Website", rowNum)));
		/*SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountShipToCreatePage
				.enterParentAccount(TestDataUtil.testData.getCellData(sheetName, "ParentAccount", rowNum)));*/
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountShipToCreatePage
				.enterAEResponsible(TestDataUtil.testData.getCellData(sheetName, "AEResponsible", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountShipToCreatePage
				.enterProductLine(TestDataUtil.testData.getCellData(sheetName, "ProductLine", rowNum)));
		/*SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountShipToCreatePage
				.enterCRCResponsible(TestDataUtil.testData.getCellData(sheetName, "CRCResponsible", rowNum)));*/
		
	}
	public static void enterShippingAddress(String sheetName, int rowNum) {
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountShipToCreatePage
				.enterShippingStreet(TestDataUtil.testData.getCellData(sheetName, "ShippingStreet", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountShipToCreatePage
				.enterShippingCity(TestDataUtil.testData.getCellData(sheetName, "ShippingCity", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountShipToCreatePage
				.enterShippingState(TestDataUtil.testData.getCellData(sheetName, "ShippingState", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountShipToCreatePage
				.enterShippingZip(TestDataUtil.testData.getCellData(sheetName, "ShippingZip", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountShipToCreatePage
				.enterShippingCountry(TestDataUtil.testData.getCellData(sheetName, "ShippingCountry", rowNum)));
	}
	
	public static void enterDescription(String sheetName, int rowNum) {
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountShipToCreatePage
				.enterDescription(TestDataUtil.testData.getCellData(sheetName, "Description", rowNum)));
	}
	
	public static void saveAccount() {
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.commonButtonPage
				.clickSaveNewAccount());
		Sync.waitForSeconds("WAIT_5");
	}
//====================================================================================================================================	
	//Create Sold To Account
	public static void createSoldToAccount(String sheetName, int rowNum) {
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.allTabsPage.clickAccountTab());
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.commonButtonPage.clickNewAccount());
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.recordTypeSelectionPage.selectSoldToRadio());
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.commonButtonPage.clickNext());
	}
	
	public static void enterSoldToAccountInformation(String sheetName, int rowNum) {
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountSoldToCreatePage
				.enterAccountName(TestDataUtil.testData.getCellData(sheetName, "AccountName", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountSoldToCreatePage
				.enterPhone(TestDataUtil.testData.getCellData(sheetName, "Phone", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountSoldToCreatePage
				.enterFax(TestDataUtil.testData.getCellData(sheetName, "Fax", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountSoldToCreatePage
				.enterRoles(TestDataUtil.testData.getCellData(sheetName, "Roles", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountSoldToCreatePage
				.enterWebsite(TestDataUtil.testData.getCellData(sheetName, "Website", rowNum)));
		
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountSoldToCreatePage
				.enterAEResponsible(TestDataUtil.testData.getCellData(sheetName, "AEResponsible", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountSoldToCreatePage
				.enterProductLine(TestDataUtil.testData.getCellData(sheetName, "ProductLine", rowNum)));
		/*SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountShipToCreatePage
				.enterCRCResponsible(TestDataUtil.testData.getCellData(sheetName, "CRCResponsible", rowNum)));*/
		
	}
	public static void enterSoldToAddress(String sheetName, int rowNum) {
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountSoldToCreatePage
				.enterStreet(TestDataUtil.testData.getCellData(sheetName, "Street", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountSoldToCreatePage
				.enterCity(TestDataUtil.testData.getCellData(sheetName, "City", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountSoldToCreatePage
				.enterState(TestDataUtil.testData.getCellData(sheetName, "State", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountSoldToCreatePage
				.enterZip(TestDataUtil.testData.getCellData(sheetName, "Zip", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountSoldToCreatePage
				.enterCountry(TestDataUtil.testData.getCellData(sheetName, "Country", rowNum)));
	}
	
	public static void enterSoldToDescription(String sheetName, int rowNum) {
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountSoldToCreatePage
				.enterDescription(TestDataUtil.testData.getCellData(sheetName, "Description", rowNum)));
	}
	
	public static void enterSoldToAccountAttributes(String sheetName, int rowNum) {
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountSoldToCreatePage
				.enterAnnualRevenue(TestDataUtil.testData.getCellData(sheetName, "AnnualRevenue", rowNum)));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.accountSoldToCreatePage
				.selectCustomerSegment(TestDataUtil.testData.getCellData(sheetName, "CustomerSegment", rowNum)));
	}

}
