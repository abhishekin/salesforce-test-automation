package com.scripts.crm360;

import org.testng.annotations.Test;

import com.common.misc.SharedDriver;
import com.common.util.CryptUtil;
import com.common.util.EnvironmentUtil;
import com.common.util.ResultUtil;
import com.common.util.TestDataUtil;
import com.config.crm360.Configuration;

public class LoginScript extends Configuration {

	@Test
	public void openApplication() {
		ResultUtil.test = ResultUtil.reporter.startTest("Open Application");
		SharedDriver.createDriver();
		ResultUtil.reporter.endTest(ResultUtil.test);
		ResultUtil.reporter.flush();
	}

	@Test(dependsOnMethods = "openApplication")
	public void loginToApplication() {
		ResultUtil.test = ResultUtil.reporter.startTest("Login to Application");
		String username = TestDataUtil.testData.getCellData("LoginAndConfig", "LoginUsername",
				EnvironmentUtil.environmentRowNum);
		String password = TestDataUtil.testData.getCellData("LoginAndConfig", "LoginPassword",
				EnvironmentUtil.environmentRowNum);
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.loginPage.enterUsername(username));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.loginPage
				.enterPassword(CryptUtil.decryptXOR(password, System.getProperty("key"))));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.loginPage.clickLoginButton());
		ResultUtil.reporter.endTest(ResultUtil.test);
		ResultUtil.reporter.flush();
	}

}
