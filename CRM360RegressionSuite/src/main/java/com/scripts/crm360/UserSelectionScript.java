package com.scripts.crm360;


import com.common.misc.SharedDriver;

public class UserSelectionScript {

	public static void selectUser(String profileName, String userName) {
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.globalHeader.clickSetupIcon());
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.globalHeader.clickSetupLink());
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.globalHeader.switchToNewTab());
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.setupPage.clickUsersLink());
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.setupPage.clickProfilesLink());
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.profilesSelectionPage.selectAlphabetIndex(profileName));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.profilesSelectionPage.selectProfile(profileName));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.profilesSelectionPage.clickViewUsersButton());
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.userSelectionPage.selectAlphabetIndex(userName));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.userSelectionPage.selectUser(userName));
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.userSelectionPage.clickLoginButton());
		SoftAssertScript.SoftAssertTrue(SharedDriver.pageContainer.userSelectionPage.waitForUserToGetLoggedin());
	}

}
